// ignore_for_file: prefer_const_constructors, unnecessary_new, use_function_type_syntax_for_parameters, unnecessary_import, unused_import

library constants;

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
  File poster = File("");
setTextFieldPassword(
  TextEditingController controller,
  String hintText,
  bool secureEntry,
  TextInputType inputType,
  bool validtion,
  String errorMSg,
  IconData suffixIconImg,
  Function onIconTap(),
  Function onchange, {required InputDecoration decoration}
) {
  return Theme(
      data: new ThemeData(
        primaryColor: Colors.green,
        primaryColorDark: Colors.red,
      ),
      child: Stack(
        children: [
          TextField(
            style: TextStyle(
                fontFamily: "Poppins-Regular",
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                color: Colors.black,
                height: 1.2),
            obscureText: secureEntry,
            enableSuggestions: false,
            autocorrect: false,
            controller: controller,
            onChanged: (value) => {
              onchange(),
            },
            keyboardType: inputType,
            decoration: InputDecoration(
              errorMaxLines: 3,
              errorText: validtion ? errorMSg : null,
              isDense: true,
              contentPadding: EdgeInsets.fromLTRB(15, 30, 0, 10),
              enabledBorder: const OutlineInputBorder(
                borderSide:  BorderSide(
                    color: Color.fromRGBO(244, 247, 250, 1), width: 1),
              ),
              border: new OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  borderSide:
                      new BorderSide(color: Color.fromRGBO(244, 247, 250, 1))),
              filled: true,
              fillColor: Colors.white,
              hintText: hintText,
              hintStyle: TextStyle(
                  fontFamily: "Poppins-Regular",
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  color: Colors.black.withAlpha(300),
                  height: 1.2),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color.fromRGBO(244, 247, 250, 1)),
              ),
            ),
            cursorColor: Colors.black,
          ),
          Positioned(
            top: 20,
            right: 15,
            child: GestureDetector(
              onTap: () {
                onIconTap();
              },
              child: Icon(
                suffixIconImg,
                size: 18,
                color: Colors.grey,
              ),
            ),
          ),
        ],
      ));
}
