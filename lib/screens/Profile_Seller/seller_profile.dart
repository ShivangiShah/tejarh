// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace, unused_import, prefer_const_literals_to_create_immutables, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/bottomnavigation.dart';
import 'package:align_positioned/align_positioned.dart';

class SellerProfile extends StatefulWidget {
  const SellerProfile({Key? key}) : super(key: key);

  @override
  State<SellerProfile> createState() => _SellerProfileState();
}

class _SellerProfileState extends State<SellerProfile> {
  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
    //   statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
    //   statusBarIconBrightness: Brightness.dark, // status bar icons' color
    //   systemNavigationBarIconBrightness:
    //       Brightness.dark, //navigation bar icons' color
    // ));

    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      height: 220.h,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: 220.h,
                            alignment: Alignment.topCenter,
                            child: Container(
                              height: 180.h,
                              width: double.infinity,
                              child: GridTile(
                                child: Image.asset(
                                  "assets/Images/seller_bg.png",
                                  fit: BoxFit.fill,
                                ),
                                header: Padding(
                                  padding: EdgeInsets.only(
                                      top: 53, left: 15, right: 15),
                                  child: Row(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: SvgPicture.asset(
                                          "assets/Icons/back.svg",
                                          color: Color(0xff262E3A),
                                          height: 21,
                                          width: 12,
                                        ),
                                      ),
                                      const Spacer(),
                                      const Text(
                                        "Profile",
                                        style: TextStyle(
                                            color: Color(0xff262E3A),
                                            fontSize: 20,
                                            fontFamily: "Poppins-SemiBold",
                                            fontWeight: FontWeight.w700),
                                        textAlign: TextAlign.center,
                                      ),
                                      Spacer(),
                                      SizedBox(width: 15.w)
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              height: 112.h,
                              width: 112.w,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                              ),
                              child: Stack(
                                children: [
                                  Center(
                                    child: Container(
                                      height: 112.h,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                      ),
                                      child: Image.asset(
                                          "assets/Images/item_avtar.png",
                                          height: 112.h),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        right: 15.0, bottom: 3),
                                    child: Align(
                                      alignment: Alignment.bottomRight,
                                      child: Container(
                                        width: 20.w,
                                        height: 20.h,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Color(0xff0AD188)),
                                            shape: BoxShape.circle,
                                            color: Color(0xff0AD188)),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "John Doe",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontFamily: "Poppins-SemiBold",
                                fontWeight: FontWeight.w600),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 9.0),
                            child: Image.asset(
                              "assets/Images/award.png",
                              height: 27.h,
                              width: 27.w,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0),
                      child: Text(
                        "Member since jan 2020",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontFamily: "Poppins-Regular",
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 3),
                      child: RatingBar.builder(
                        initialRating: 3,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemSize: 20,
                        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (rating) {
                          print(rating);
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset("assets/Icons/Location.svg"),
                          SizedBox(width: 10),
                          Text(
                            "Riyadh, Saudi Arabia",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontFamily: "Poppins-Regular",
                                fontWeight: FontWeight.w400),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 22),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IntrinsicHeight(
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Container(
                                  width: 60.w,
                                  child: Column(
                                    children: [
                                      Text(
                                        "10",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 30,
                                            fontFamily: "Poppins-SemiBold",
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "Bought",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                            fontFamily: "Poppins-Regular",
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                                VerticalDivider(
                                  color: Colors.grey,
                                  thickness: 2,
                                ),
                                Container(
                                  width: 60.w,
                                  child: Column(
                                    children: [
                                      Text(
                                        "20",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 30,
                                            fontFamily: "Poppins-SemiBold",
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "Sold",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                            fontFamily: "Poppins-Regular",
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                                VerticalDivider(
                                  color: Colors.grey,
                                  thickness: 2,
                                ),
                                Container(
                                  width: 60.w,
                                  child: Column(
                                    children: [
                                      Text(
                                        "5",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 30,
                                            fontFamily: "Poppins-SemiBold",
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "Followers",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                            fontFamily: "Poppins-Regular",
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                                VerticalDivider(
                                  color: Colors.grey,
                                  thickness: 2,
                                ),
                                Container(
                                  width: 60.w,
                                  child: Column(
                                    children: [
                                      Text(
                                        "20",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 30,
                                            fontFamily: "Poppins-SemiBold",
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        "Following",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                            fontFamily: "Poppins-Regular",
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 15.0, right: 15, top: 20),
                      child: Container(
                        height: 57.h,
                        width: double.infinity,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(7)),
                          onPressed: () {
                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(builder: (context) => WishList()),
                            // );
                          },
                          color: const Color.fromARGB(239, 66, 190, 138),
                          child: const Text("Follow",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular")),
                        ),
                      ),
                    ),
                    Container(
                      // width: double.infinity,
                      // color: Colors.amber,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 20.0, right: 15, left: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Image.asset("assets/Icons/mystory.png",
                                    height: 54.h, width: 54.w),
                                SizedBox(height: 7.h),
                                Text(
                                  "My Story",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontFamily: "Poppins-Regular",
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              height: 82.h,
                              // width: double.infinity,
                              // height: 100.h,
                              // color: Colors.blue,
                              width: MediaQuery.of(context).size.width - 90.w,
                              // height: MediaQuery.of(context).size.height * 0.150.w,
                              child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: 10,
                                  shrinkWrap: true,
                                  physics: ScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    return Padding(
                                      padding: const EdgeInsets.only(
                                        left: 5,
                                      ),
                                      child: Container(
                                        // color: Colors.green,
                                        height: 82.h,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              height: 56.h,
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Color(0xff0AD188),
                                                    width: 2.0),
                                                shape: BoxShape.circle,
                                                color: Colors.white,
                                              ),
                                              child: Center(
                                                child: Image.asset(
                                                    "assets/Icons/Jessy314.png",
                                                    height: 54.h,
                                                    width: 54.w),
                                              ),
                                            ),
                                            SizedBox(height: 7.h),
                                            Text(
                                              "Jessy314",
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12,
                                                fontFamily: "Poppins-Regular",
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  }),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20, left: 15.0, right: 15),
                      child: Divider(thickness: 2),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 15.0, right: 15, top: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Verified profile by",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontFamily: "Poppins-SemiBold",
                                fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Image.asset(
                                "assets/Icons/email.png",
                                height: 30.h,
                              ),
                              SizedBox(height: 5.h),
                              Text(
                                "Confirmed",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontFamily: "Poppins-Regular",
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Image.asset(
                                "assets/Icons/call.png",
                                height: 30.h,
                              ),
                              SizedBox(height: 5.h),
                              Text(
                                "Confirmed",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontFamily: "Poppins-Regular",
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Image.asset(
                                "assets/Icons/facebook.png",
                                height: 30.h,
                              ),
                              SizedBox(height: 5.h),
                              Text(
                                "Confirmed",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontFamily: "Poppins-Regular",
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Image.asset(
                                "assets/Icons/id-card.png",
                                height: 30.h,
                              ),
                              SizedBox(height: 5.h),
                              Text(
                                "Unconfirmed",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontFamily: "Poppins-Regular",
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20, left: 15.0, right: 15),
                      child: Divider(thickness: 2),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 15.0, right: 15, top: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Verified seller badge",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontFamily: "Poppins-SemiBold",
                                fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 15.0),
                            child: Column(
                              children: [
                                Image.asset(
                                  "assets/Icons/calendar.png",
                                  height: 30.h,
                                ),
                                SizedBox(height: 5.h),
                                Text(
                                  "Member Since \n 1 year",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontFamily: "Poppins-Regular",
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                          Column(
                            children: [
                              Image.asset(
                                "assets/Icons/truck.png",
                                height: 30.h,
                              ),
                              SizedBox(height: 5.h),
                              Text(
                                "Quick Shipper \n     ",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontFamily: "Poppins-Regular",
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 15.0),
                            child: Column(
                              children: [
                                Image.asset(
                                  "assets/Icons/security.png",
                                  height: 30.h,
                                ),
                                SizedBox(height: 5.h),
                                Text(
                                  "Reliable \n    ",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontFamily: "Poppins-Regular",
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20, left: 15.0, right: 15),
                      child: Divider(thickness: 2),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 15.0, right: 15, top: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Items from this seller",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontFamily: "Poppins-SemiBold",
                                fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      child: Container(
                        // height: 470.h,
                        // color: Colors.green,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SingleChildScrollView(
                              child: Container(
                                // height: 470.h,
                                // height: MediaQuery.of(context).size.height * 0.58,
                                // decoration: BoxDecoration(color: Colors.white),
                                child: GridView.builder(
                                    shrinkWrap: true,
                                    physics: ScrollPhysics(),
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      childAspectRatio: 0.80.r,
                                      mainAxisSpacing: 10.r,
                                      crossAxisSpacing: 20.0.r,
                                    ),
                                    itemCount: 4,
                                    itemBuilder: (BuildContext ctx, index) {
                                      return Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: 160.w,
                                            decoration: BoxDecoration(
                                              color: Color.fromARGB(
                                                  198, 255, 255, 255),
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              boxShadow: const [
                                                BoxShadow(
                                                    color: Color.fromARGB(
                                                        255, 221, 216, 216),
                                                    blurRadius: 5,
                                                    spreadRadius: 1,
                                                    offset: Offset(4, 4)),
                                              ],
                                            ),
                                            child: Wrap(
                                              children: [
                                                Stack(
                                                  children: [
                                                    Image.asset(
                                                      "assets/Images/airpods.png",
                                                    ),
                                                    Column(
                                                      children: [
                                                        Align(
                                                          alignment: Alignment
                                                              .topRight,
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 11,
                                                                    right: 12),
                                                            child: Container(
                                                                width: 20.w,
                                                                height: 20.h,
                                                                decoration: BoxDecoration(
                                                                    shape: BoxShape
                                                                        .circle,
                                                                    color: Color
                                                                        .fromARGB(
                                                                            255,
                                                                            216,
                                                                            206,
                                                                            206)),
                                                                child: Center(
                                                                  child: SvgPicture.asset(
                                                                      "assets/Icons/heart.svg",
                                                                      height:
                                                                          11.h,
                                                                      width:
                                                                          12.w),
                                                                )),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 11),
                                                      child: Container(
                                                        height: 20.h,
                                                        width: 65.w,
                                                        decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.only(
                                                                topRight: Radius
                                                                    .circular(
                                                                        10.0),
                                                                bottomRight: Radius
                                                                    .circular(
                                                                        10.0)),
                                                            color:
                                                                Color.fromARGB(
                                                                    239,
                                                                    66,
                                                                    190,
                                                                    138)),
                                                        child: Center(
                                                          child: Text(
                                                              "FEATURED",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 10,
                                                                fontFamily:
                                                                    "Poppins-Medium",
                                                              )),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Container(
                                                  // height: 70.h,
                                                  // color: Colors.grey,
                                                  child: Column(
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                top: 5,
                                                                left: 8,
                                                                right: 5),
                                                        child: Row(
                                                          children: [
                                                            Text(
                                                              "7,000 SAR",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 14,
                                                                fontFamily:
                                                                    "Poppins-SemiBold",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ),
                                                            ),
                                                            Spacer(),
                                                            Container(
                                                              height: 18.h,
                                                              width: 37.w,
                                                              decoration: BoxDecoration(
                                                                  color: Color
                                                                      .fromARGB(
                                                                          255,
                                                                          221,
                                                                          30,
                                                                          30),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              8)),
                                                              child: Center(
                                                                child: Text(
                                                                  "USED",
                                                                  style:
                                                                      TextStyle(
                                                                    color: Color
                                                                        .fromARGB(
                                                                            255,
                                                                            248,
                                                                            246,
                                                                            246),
                                                                    fontSize:
                                                                        10,
                                                                    fontFamily:
                                                                        "Poppins-Regular",
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                top: 1,
                                                                left: 8,
                                                                right: 5),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              "Apple airpods",
                                                              textAlign:
                                                                  TextAlign
                                                                      .left,
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 12,
                                                                fontFamily:
                                                                    "Poppins-Regular",
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                top: 2,
                                                                left: 8,
                                                                right: 5),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              "Jeddah, Saudi Arabia",
                                                              textAlign:
                                                                  TextAlign
                                                                      .left,
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 10,
                                                                fontFamily:
                                                                    "Poppins-Regular",
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      // Container(
                                                      //     height: 2.h,
                                                      //     color: Colors.grey),
                                                      Divider(),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 8,
                                                                right: 5,
                                                                bottom: 2),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Image.asset(
                                                                "assets/Icons/location_icon.png",
                                                                width: 16.w,
                                                                height: 17.h),
                                                            SizedBox(
                                                              height: 17.h,
                                                              width: 5.w,
                                                            ),
                                                            Text(
                                                              "The Full Cart",
                                                              textAlign:
                                                                  TextAlign
                                                                      .left,
                                                              style: TextStyle(
                                                                color: Color
                                                                    .fromARGB(
                                                                        239,
                                                                        66,
                                                                        190,
                                                                        138),
                                                                fontSize: 10,
                                                                fontFamily:
                                                                    "Poppins-Regular",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700,
                                                              ),
                                                            ),
                                                            Spacer(),
                                                            Container(
                                                                height: 10.h,
                                                                width: 10.w,
                                                                decoration: BoxDecoration(
                                                                    color: Color
                                                                        .fromARGB(
                                                                            239,
                                                                            66,
                                                                            190,
                                                                            138),
                                                                    shape: BoxShape
                                                                        .circle)),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      );
                                    }),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: BottomMenu(
                selectedIndex: 4,
              ),
            )
          ],
        ));
  }
}
