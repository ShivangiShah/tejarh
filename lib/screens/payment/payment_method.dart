// ignore_for_file: prefer_const_constructors, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/payment/addcard.dart';
import 'package:tejarh/screens/payment/ordersuccessful.dart';

class PaymentMethod extends StatefulWidget {
  const PaymentMethod({Key? key}) : super(key: key);

  @override
  State<PaymentMethod> createState() => _PaymentMethodState();
}

class _PaymentMethodState extends State<PaymentMethod> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
            body: SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 9),
            child: Row(
              children: [
                InkWell(
                  // close keyboard on outside input tap
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: SvgPicture.asset(
                    "assets/Icons/back.svg",
                    height: 21,
                    width: 12, //just like you define in pubspec.yaml file
                  ),
                ),
                Spacer(),
                Text(
                  "Payment Method",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontFamily: "Poppins-SemiBold",
                      fontWeight: FontWeight.w700),
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                SizedBox(
                  height: 21,
                  width: 12,
                ),
              ],
            ),
          ),
          Container(
              height: MediaQuery.of(context).size.height,
              child: Image.asset(
                "assets/Images/paymentmethod.png",
                fit: BoxFit.cover,
              )),
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddCard()),
              );
            },
            child: Text(
              "Add card",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontFamily: "Poppins-SemiBold",
                  fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 15, bottom: 20),
            child: Container(
              height: 57.h,
              width: double.infinity,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7)),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => OrderSuccessful()),
                  );
                },
                color: const Color.fromARGB(239, 66, 190, 138),
                child: const Text("Pay Now",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontFamily: "Poppins-Regular")),
              ),
            ),
          ),
        ],
      ),
    )));
  }
}
