// screen 4.2  Offer Accept/Negotiate - Seller

// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, deprecated_member_use, non_constant_identifier_names, unused_import

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/chat/chattingpage.dart';
import 'package:tejarh/screens/Homescreen/Category/ask.dart';
import 'package:tejarh/screens/payment/negotiate_seller.dart';


class OfferAccept extends StatefulWidget {
  const OfferAccept({Key? key}) : super(key: key);

  @override
  State<OfferAccept> createState() => _OfferAcceptState();
}

class _OfferAcceptState extends State<OfferAccept> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));
    BottomNav() {
      return showModalBottomSheet(
        context: context,
        barrierColor: Color.fromARGB(157, 146, 144, 144),
        backgroundColor: Colors.white,
        elevation: 20,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
        ),
        builder: (BuildContext context) {
          return Expanded(
              child: SingleChildScrollView(
                  child: Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 15),
            child: Column(children: [
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: const Text(
                  'Offer Received',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontFamily: "Poppins-SemiBold",
                      fontWeight: FontWeight.w700),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Text(
                  "You have recived a new offer for your ad \nApple airpods",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Image.asset("assets/Images/airpods.png",
                        height: 66.h, width: 120.w),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Apple airpods",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: "Poppins-SemiBold",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Row(
                            children: [
                              Text(
                                "7,000 SAR",
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 16,
                                  decoration: TextDecoration.lineThrough,
                                  fontFamily: "Poppins-Regular",
                                  // fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(width: 5.w),
                              Text(
                                "6,000 SAR",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, bottom: 20),

                child: Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChattingPage()),
                          );
                        },
                        child: Container(
                          height: 57.h,
                          decoration: BoxDecoration(
                            border: Border.all(color: Color(0xff0AD188)),
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Center(
                            child: Text(
                              "Accept",
                              style: TextStyle(
                                color: Color(0xff0AD188),
                                fontSize: 16,
                                fontFamily: "Poppins-Regular",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 15.w),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NegotiateSeller()),
                          );
                        },
                        child: Container(
                          height: 57.h,
                          decoration: BoxDecoration(
                            color: Color(0xff0AD188),
                            border: Border.all(color: Color(0xff0AD188)),
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Center(
                            child: Text(
                              "Negotiate",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontFamily: "Poppins-Regular",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                // child: Container(
                //   height: 57.h,
                //   width: double.infinity,
                //   child: RaisedButton(
                //     shape: RoundedRectangleBorder(
                //         borderRadius: BorderRadius.circular(7)),
                //     onPressed: () {
                //       Navigator.push(
                //         context,
                //         MaterialPageRoute(builder: (context) => Ask()),
                //       );
                //     },
                //     color: const Color.fromARGB(239, 66, 190, 138),
                //     child: const Text("Post",
                //         style: TextStyle(
                //             color: Colors.white,
                //             fontSize: 16,
                //             fontFamily: "Poppins-Regular")),
                //   ),
                // ),
              )
            ]),
          )));
        },
      );
    }

    return SafeArea(
        child: Scaffold(
      body: InkWell(
        onTap: (BottomNav),
        child: Center(
          child: Container(
            height: 25.h,
            width: 30.w,
            decoration: BoxDecoration(color: Colors.green),
          ),
        ),
      ),
    ));
  }
}
