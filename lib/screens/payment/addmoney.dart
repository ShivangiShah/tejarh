// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, deprecated_member_use, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/wallet/wallet.dart';

class AddMoney extends StatefulWidget {
  const AddMoney({Key? key}) : super(key: key);

  @override
  State<AddMoney> createState() => _AddMoneyState();
}

class _AddMoneyState extends State<AddMoney> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
            body: Column(children: [
      Expanded(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 9.0, left: 15, right: 15),
              child: Row(
                children: [
                  InkWell(
                    // close keyboard on outside input tap
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: SvgPicture.asset(
                      "assets/Icons/back.svg",
                      height: 21,
                      width: 12, //just like you define in pubspec.yaml file
                    ),
                  ),
                  Spacer(),
                  const Text(
                    "Add Money",
                    style: TextStyle(
                        color: Color(0xff262E3A),
                        fontSize: 20,
                        fontFamily: "Poppins-SemiBold",
                        fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                  const Spacer(),
                  SizedBox(
                    width: 12,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30.0, left: 15, right: 15),
              child: TextFormField(
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Add Money',
                ),
              ),
            ),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(left: 15.0, right: 15, bottom: 20),
        child: Container(
          height: 57.h,
          width: double.infinity,
          child: RaisedButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Wallet()),
              );
            },
            color: const Color.fromARGB(239, 66, 190, 138),
            child: const Text("Add Money",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontFamily: "Poppins-Regular")),
          ),
        ),
      ),
    ])));
  }
}
