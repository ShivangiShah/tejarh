// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OrderDetails extends StatefulWidget {
  const OrderDetails({Key? key}) : super(key: key);

  @override
  State<OrderDetails> createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
            body: Padding(
      padding: const EdgeInsets.only(left: 15.0, right: 15),
      child: Column(children: [
        Padding(
          padding: EdgeInsets.only(top: 9),
          child: Row(
            children: [
              InkWell(
                // close keyboard on outside input tap
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  "assets/Icons/back.svg",
                  height: 21,
                  width: 12, //just like you define in pubspec.yaml file
                ),
              ),
              Spacer(),
              Text(
                "Order Details",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontFamily: "Poppins-SemiBold",
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              ),
              Spacer(),
              SizedBox(
                height: 21,
                width: 12,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Container(
            width: double.infinity,
            height: 52.h,
            decoration: BoxDecoration(
              color: Color(0xff009D25),
              border: Border.all(color: Color(0xff009D25)),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Center(
              child: Text(
                "Order Delivered",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                    fontWeight: FontWeight.w400),
                // textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 13.0),
          child: Row(
            children: [
              Text(
                "Shipping to",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: "Poppins-SemiBold",
                    fontWeight: FontWeight.w600),
                textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
        Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              SvgPicture.asset("assets/Icons/Location.svg",
                  width: 21.w, height: 26.h),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(
                  "P.O Box 401247, \n Dubai",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontFamily: "Poppins-Regular",
                      fontWeight: FontWeight.w500),
                  textAlign: TextAlign.left,
                ),
              ),
            ])),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Divider(thickness: 2),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.asset(
                "assets/Images/airpods.png",
                height: 100.h,
                // width: 146.w,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Column(
                  children: [
                    Text(
                      "Apple airpods",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontFamily: "Poppins-Regular",
                          fontWeight: FontWeight.w400),
                      // textAlign: TextAlign.left,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(
                        "7,000 SAR",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontFamily: "Poppins-SemiBold",
                            fontWeight: FontWeight.w600),
                        // textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Divider(
            thickness: 2,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Payment Details",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: "Poppins-SemiBold",
                  fontWeight: FontWeight.w600),
              // textAlign: TextAlign.left,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Row(
            children: [
              Text(
                "Item price",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                    fontWeight: FontWeight.w400),
                // textAlign: TextAlign.left,
              ),
              Spacer(),
              Text(
                "7,000 SAR",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                    fontWeight: FontWeight.w400),
                // textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 12.0),
          child: Row(
            children: [
              Text(
                "Sell tax",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                    fontWeight: FontWeight.w400),
                // textAlign: TextAlign.left,
              ),
              Spacer(),
              Text(
                "50 SAR",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                    fontWeight: FontWeight.w400),
                // textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Row(
            children: [
              Text(
                "Shipping Charge",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                    fontWeight: FontWeight.w400),
                // textAlign: TextAlign.left,
              ),
              Spacer(),
              Text(
                "10 SAR",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                    fontWeight: FontWeight.w400),
                // textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
        Divider(
          thickness: 2,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 12.0),
          child: Row(
            children: [
              Text(
                "Amount Payable",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontFamily: "Poppins-Regular",
                    fontWeight: FontWeight.w600),
                // textAlign: TextAlign.left,
              ),
              Spacer(),
              Text(
                "7,060 SAR",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontFamily: "Poppins-Regular",
                    fontWeight: FontWeight.w600),
                // textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Container(
            width: double.infinity,
            height: 57.h,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Color(0xff009D25)),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Center(
              child: Text(
                "Download Invoice",
                style: TextStyle(
                    color: Color(0xff009D25),
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                    fontWeight: FontWeight.w400),
                // textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: RatingBar.builder(
            initialRating: 3,
            minRating: 1,
            direction: Axis.horizontal,
            allowHalfRating: true,
            itemCount: 5,
            itemSize: 50,
            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
            itemBuilder: (context, _) => Icon(
              Icons.star,
              color: Colors.amber,
            ),
            onRatingUpdate: (rating) {
              print(rating);
            },
          ),
        ),
      ]),
    )));
  }
}
