// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, avoid_unnecessary_containers, deprecated_member_use, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/payment/payment_method.dart';

class AddCard extends StatefulWidget {
  const AddCard({Key? key}) : super(key: key);

  @override
  State<AddCard> createState() => _AddCardState();
}

class _AddCardState extends State<AddCard> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
            body: SingleChildScrollView(
      child: Column(children: [
        Padding(
          padding: const EdgeInsets.only(top: 9.0, left: 15, right: 15),
          child: Row(
            children: [
              InkWell(
                // close keyboard on outside input tap
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  "assets/Icons/back.svg",
                  height: 21,
                  width: 12, //just like you define in pubspec.yaml file
                ),
              ),
              Spacer(),
              const Text(
                "Add Card",
                style: TextStyle(
                    color: Color(0xff262E3A),
                    fontSize: 20,
                    fontFamily: "Poppins-SemiBold",
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              ),
              const Spacer(),
              SizedBox(
                width: 12,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 22, left: 20, right: 20),
          child: Image.asset("assets/Images/card.png"),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 30.0, right: 15, left: 15),
          child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Card Holder Name',
                hintStyle: TextStyle(color: Colors.grey)),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 15.0, right: 15, left: 15),
          child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter Card Number',
                hintStyle: TextStyle(color: Colors.grey)),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 15.0, left: 15, right: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      border: Border.all(
                        color: Colors.grey,
                      )),
                  width: 164.w,
                  height: 51.h,
                  child: Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          maxLength: 2,
                          textAlign: TextAlign.center,
                          // autofocus: true,
                          initialValue: '',
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'MM',
                            counterText: "",
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          ),
                        ),
                      ),
                      Container(
                        width: 1.w,
                        height: 53.h,
                        decoration: BoxDecoration(color: Colors.grey),
                      ),
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          maxLength: 4,
                          textAlign: TextAlign.center,
                          // autofocus: true,
                          initialValue: '',
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'YY',
                            counterText: "",
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(width: 17.w),
              Expanded(
                child: Container(
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    maxLength: 3,
                    textAlign: TextAlign.center,
                    initialValue: '',
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'CVV',
                      counterText: "",
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 15.0, right: 15, top: 20),
          child: Container(
            height: 57.h,
            width: double.infinity,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7)),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PaymentMethod()),
                );
              },
              color: const Color.fromARGB(239, 66, 190, 138),
              child: const Text("Add Card",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: "Poppins-Regular")),
            ),
          ),
        ),
      ]),
    )));
  }
}
