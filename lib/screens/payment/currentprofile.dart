// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:tejarh/screens/Homescreen/Bottomnevigation/bottomnavigation.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myprofile/editprofile.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myorders/myorders.dart';

import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myitems/myitems.dart';

class CurrentProfile extends StatefulWidget {
  const CurrentProfile({Key? key}) : super(key: key);

  @override
  State<CurrentProfile> createState() => _CurrentProfileState();
}

class _CurrentProfileState extends State<CurrentProfile> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            body: Column(children: [
              Expanded(
                  child: SingleChildScrollView(
                      child: Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15),
                child: Column(children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 9.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 12,
                        ),
                        Spacer(),
                        const Text(
                          "Profile",
                          style: TextStyle(
                              color: Color(0xff262E3A),
                              fontSize: 20,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        ),
                        const Spacer(),
                        InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EditProfile()),
                              );
                            },
                            child: Icon(Icons.edit)),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 23.0),
                    child: Stack(
                      children: [
                        Center(
                          child: Container(
                            width: 112.w,
                            child: Center(
                              child: Image.asset("assets/Images/item_avtar.png",
                                  height: 112.h),
                            ),
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(top: 100),
                            child: Align(
                                alignment: Alignment.bottomCenter,
                                child: SvgPicture.asset(
                                    "assets/Icons/online.svg",
                                    height: 26.h,
                                    width: 65.w))),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "John Doe",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w500),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Icon(
                            Icons.verified,
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 3.0),
                    child: Text(
                      "Member since jan 2020",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontFamily: "Poppins-Regular",
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 6),
                    child: RatingBar.builder(
                      initialRating: 3,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemSize: 22,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        print(rating);
                      },
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(top: 24),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            IntrinsicHeight(
                                child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                  Container(
                                    width: 120.w,
                                    child: Column(
                                      children: [
                                        Text(
                                          "5",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 30,
                                              fontFamily: "Poppins-SemiBold",
                                              fontWeight: FontWeight.w600),
                                        ),
                                        Text(
                                          "Followers",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12,
                                              fontFamily: "Poppins-Regular",
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ],
                                    ),
                                  ),
                                  VerticalDivider(
                                    color: Colors.grey,
                                    thickness: 2,
                                  ),
                                  Container(
                                    width: 120.w,
                                    child: Column(
                                      children: [
                                        Text(
                                          "10",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 30,
                                              fontFamily: "Poppins-SemiBold",
                                              fontWeight: FontWeight.w600),
                                        ),
                                        Text(
                                          "Following",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12,
                                              fontFamily: "Poppins-Regular",
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ],
                                    ),
                                  ),
                                ]))
                          ])),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Divider(
                      thickness: 2,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Verified your account",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Image.asset(
                              "assets/Icons/email.png",
                              height: 30.h,
                            ),
                            SizedBox(height: 5.h),
                            Text(
                              "  Email \n Verified",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontFamily: "Poppins-Regular",
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Image.asset(
                              "assets/Icons/call.png",
                              height: 30.h,
                            ),
                            SizedBox(height: 5.h),
                            Text(
                              "  Phone \n Verified",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontFamily: "Poppins-Regular",
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Image.asset(
                              "assets/Icons/id-card.png",
                              height: 30.h,
                            ),
                            SizedBox(height: 5.h),
                            Text(
                              "Government \n    Verified",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontFamily: "Poppins-Regular",
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Divider(
                      thickness: 2,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Verified seller badge",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 15.0),
                          child: Column(
                            children: [
                              Image.asset(
                                "assets/Icons/calendar.png",
                                height: 30.h,
                              ),
                              SizedBox(height: 5.h),
                              Text(
                                "Member Since \n 1 year",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontFamily: "Poppins-Regular",
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            Image.asset(
                              "assets/Icons/truck.png",
                              height: 30.h,
                            ),
                            SizedBox(height: 5.h),
                            Text(
                              "Quick Shipper \n     ",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontFamily: "Poppins-Regular",
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 15.0),
                          child: Column(
                            children: [
                              Image.asset(
                                "assets/Icons/security.png",
                                height: 30.h,
                              ),
                              SizedBox(height: 5.h),
                              Text(
                                "Reliable \n    ",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontFamily: "Poppins-Regular",
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Divider(
                      thickness: 2,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MyOrders()),
                        );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "My Orders",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontFamily: "Poppins-Medium",
                                fontWeight: FontWeight.w400),
                          ),
                          Spacer(),
                          Icon(Icons.arrow_forward_ios_outlined)
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Divider(thickness: 2),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MyItems()),
                        );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "My Items",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontFamily: "Poppins-Medium",
                                fontWeight: FontWeight.w400),
                          ),
                          Spacer(),
                          Icon(Icons.arrow_forward_ios_outlined)
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Divider(thickness: 2),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: InkWell(
                      onTap: () {
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(builder: (context) => Wallet()),
                        // );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Wallet",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontFamily: "Poppins-Medium",
                                fontWeight: FontWeight.w400),
                          ),
                          Spacer(),
                          Icon(Icons.arrow_forward_ios_outlined)
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Divider(thickness: 2),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20, bottom: 20),
                    child: InkWell(
                      onTap: () {
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(builder: (context) => Wallet()),
                        // );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Settings",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontFamily: "Poppins-Medium",
                                fontWeight: FontWeight.w400),
                          ),
                          Spacer(),
                          Icon(Icons.arrow_forward_ios_outlined)
                        ],
                      ),
                    ),
                  ),
                ]),
              ))),
              Align(
                alignment: Alignment.bottomCenter,
                child: BottomMenu(
                  selectedIndex: 4,
                ),
              )
            ])));
  }
}
