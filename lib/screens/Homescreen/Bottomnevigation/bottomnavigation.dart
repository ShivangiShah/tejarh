// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unused_local_variable, must_be_immutable, use_key_in_widget_constructors, unused_import, avoid_print, dead_code, unused_label, deprecated_member_use, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';


import 'package:tejarh/screens/Homescreen/Bottomnevigation/chat/chat.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myorders/trackorder.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myprofile/myprofile.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/wishlist/wishlist.dart';

import 'package:tejarh/screens/Homescreen/Bottomnevigation/homepage/homepage.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myorders/myorders.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/post%20an%20item/post_an_item.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/hold%20an%20offer/holdanoffer.dart';

import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myitems/myitems.dart';

import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myorders/returnitem.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/wallet/wallet.dart';

import 'package:tejarh/widgets/mydrawer.dart';

class BottomMenu extends StatefulWidget {
  int? selectedIndex = 0;
  BottomMenu({
    this.selectedIndex,
  });

  @override
  State<BottomMenu> createState() => _BottomMenuState();
}

class _BottomMenuState extends State<BottomMenu> {
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
    print(widget.selectedIndex);
    _currentIndex = widget.selectedIndex!;
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Theme(
            data: Theme.of(context).copyWith(
              canvasColor: const Color.fromARGB(255, 27, 25, 35),
            ), //
            child: Container(
              height: 65.h,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(40.0),
                  topLeft: Radius.circular(40.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 5,
                    blurRadius: 2,
                    offset: const Offset(1, 5), // changes position of shadow
                  ),
                ],
              ),
              child: ClipRRect(
                child: BottomNavigationBar(
                  //selectedItemColor: Colors.red,
                  //selectedIconTheme: IconThemeData(color: Colors.red),
                  type: BottomNavigationBarType.fixed,
                  showSelectedLabels: false,
                  showUnselectedLabels: false,

                  backgroundColor: Color.fromARGB(255, 255, 255, 255),
                  onTap: onTabTapped,
                  currentIndex: _currentIndex,
                  items: [
                    BottomNavigationBarItem(
                        activeIcon: Column(
                          children: [
                            (SvgPicture.asset(
                              "assets/Icons/home.svg",
                              color: const Color.fromARGB(239, 66, 190, 138),
                            )),
                          ],
                        ),
                        icon: (SvgPicture.asset(
                          "assets/Icons/home.svg",
                          color: Color.fromARGB(255, 0, 0, 0),
                        )),
                        label: ''),
                    BottomNavigationBarItem(
                        activeIcon: (SvgPicture.asset(
                          "assets/Icons/chat.svg",
                          color: const Color.fromARGB(239, 66, 190, 138),
                        )),
                        icon: (SvgPicture.asset(
                          "assets/Icons/chat.svg",
                          color: Color.fromARGB(255, 0, 0, 0),
                        )),
                        label: ''),
                    BottomNavigationBarItem(
                        activeIcon: (SvgPicture.asset(
                          "assets/Icons/camera.svg",
                          color: const Color.fromARGB(239, 66, 190, 138),
                        )),
                        icon: (SvgPicture.asset(
                          "assets/Icons/camera.svg",
                          color: Color.fromARGB(255, 0, 0, 0),
                        )),
                        label: ''),
                    BottomNavigationBarItem(
                        activeIcon: (SvgPicture.asset(
                          "assets/Icons/myorders.svg",
                          color: const Color.fromARGB(239, 66, 190, 138),
                        )),
                        icon: (SvgPicture.asset(
                          "assets/Icons/myorders.svg",
                          color: Color.fromARGB(255, 0, 0, 0),
                        )),
                        label: ''),
                    BottomNavigationBarItem(
                        activeIcon: (SvgPicture.asset(
                          "assets/Icons/account.svg",
                          color: const Color.fromARGB(239, 66, 190, 138),
                        )),
                        icon: (SvgPicture.asset(
                          "assets/Icons/account.svg",
                          color: Color.fromARGB(255, 0, 0, 0),
                        )),
                        label: ''),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
      if (index == 0) {
        Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) => HomePage(),
              transitionDuration: Duration.zero,
              reverseTransitionDuration: Duration.zero,
            ));

        // Get.to(HomePage());
      } else if (index == 1) {
        Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) => Chat(),
              transitionDuration: Duration.zero,
              reverseTransitionDuration: Duration.zero,
            ));
        // Get.to(Chat());
      } else if (index == 2) {
        widget.selectedIndex = 2;
        Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) => PostAnItem(),
              transitionDuration: Duration.zero,
              reverseTransitionDuration: Duration.zero,
            ));
        // Get.to(PostAnItem());
      } else if (index == 3) {
        widget.selectedIndex = 3;
        Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) => MyOrders(),
              transitionDuration: Duration.zero,
              reverseTransitionDuration: Duration.zero,
            ));
      } else if (index == 4) {
        widget.selectedIndex = 4;
        Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) => MyDrawer(),
              transitionDuration: Duration.zero,
              reverseTransitionDuration: Duration.zero,
            ));
      }
    });
  }
}
