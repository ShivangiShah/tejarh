// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, deprecated_member_use, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/payment/payment_method.dart';

class BoostItem extends StatefulWidget {
  BoostItem({Key? key}) : super(key: key);

  @override
  State<BoostItem> createState() => _BoostItemState();
}

class _BoostItemState extends State<BoostItem> {
  var rating = 3.0;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    BottomSheet() {
      return showModalBottomSheet(
        context: context,
        barrierColor: Color.fromARGB(157, 146, 144, 144),
        backgroundColor: Colors.white,
        elevation: 20,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
        ),
        builder: (BuildContext context) {
          return Wrap(
            children: [
              Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 40.0),
                      child: Text(
                        "Boost your Item that helps your \nproduct to show by more buyer",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: "Poppins-Regular",
                            fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: Text(
                        "100 SAR",
                        style: TextStyle(
                            color: Color.fromARGB(255, 53, 216, 140),
                            fontSize: 29,
                            fontFamily: "Poppins-SemiBold",
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 25.0),
                      child: Text(
                        "for boost your item",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 23,
                            fontFamily: "Poppins-Regular",
                            fontWeight: FontWeight.w500),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 45.0, bottom: 10),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PaymentMethod()),
                          );
                        },
                        child: Container(
                          height: 57.h,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Color(0xff0AD188),
                            border: Border.all(color: Color(0xff0AD188)),
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Center(
                            child: Text(
                              "Pay Now",
                              style: TextStyle(
                                color: Color.fromARGB(255, 255, 255, 255),
                                fontSize: 16,
                                fontFamily: "Poppins-Regular",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ])),
            ],
          );
        },
      );
    }

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 9, left: 15, right: 15),
                child: Row(
                  children: [
                    InkWell(
                      // close keyboard on outside input tap
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: SvgPicture.asset(
                        "assets/Icons/back.svg",
                        height: 21,
                        width: 12, //just like you define in pubspec.yaml file
                      ),
                    ),
                    const Spacer(),
                    const Text(
                      "Items",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontFamily: "Poppins-SemiBold",
                          fontWeight: FontWeight.w700),
                      textAlign: TextAlign.center,
                    ),
                    const Spacer(),
                    InkWell(
                        onTap: () {
                          // Navigator.push(
                          //   context,
                          //   MaterialPageRoute(
                          //       builder: (context) =>
                          //           EditProfile()),
                          // );
                        },
                        child: Icon(Icons.edit)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 18.0),
                child: ImageSlideshow(
                  width: double.infinity,
                  height: 200,
                  initialPage: 0,
                  indicatorColor: Color.fromARGB(234, 55, 231, 158),
                  indicatorBackgroundColor: Colors.grey,
                  onPageChanged: (value) {
                    debugPrint('Page changed: $value');
                  },
                  autoPlayInterval: 3000,
                  isLoop: true,
                  children: [
                    Image.asset(
                      'assets/Images/item.png',
                      fit: BoxFit.cover,
                    ),
                    Image.asset(
                      'assets/Images/item.png',
                      fit: BoxFit.cover,
                    ),
                    Image.asset(
                      'assets/Images/item.png',
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 15.0, left: 15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 33.0),
                      child: Row(
                        children: [
                          Text(
                            "Apple airpods",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-Bold",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(
                            width: 11.w,
                          ),
                          Container(
                            height: 18.h,
                            width: 37.w,
                            decoration: BoxDecoration(
                                color: Color.fromARGB(255, 221, 30, 30),
                                borderRadius: BorderRadius.circular(8)),
                            child: Center(
                              child: Text(
                                "USED",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 248, 246, 246),
                                  fontSize: 10,
                                  fontFamily: "Poppins-Regular",
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 41.w),
                          Image.asset(
                            'assets/Icons/like.png',
                            height: 20.h,
                            width: 20.w,
                          ),
                          SizedBox(width: 15.w),
                          Image.asset(
                            'assets/Icons/eye.png',
                            height: 20.h,
                            width: 20.w,
                          ),
                          SizedBox(width: 3.w),
                          Text(
                            "2K",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontFamily: "Poppins-Bold",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(width: 15.w),
                          Image.asset(
                            'assets/Icons/waving-flag-.png',
                            height: 20.h,
                            width: 20.w,
                          ),
                          SizedBox(width: 15.w),
                          Image.asset(
                            'assets/Icons/sharing.png',
                            height: 20.h,
                            width: 20.w,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 8.0,
                      ),
                      child: Row(
                        children: [
                          Image.asset("assets/Icons/dollar-tag.png",
                              height: 22.h, width: 22.w),
                          SizedBox(
                            width: 3.w,
                          ),
                          Text(
                            "7,000 SAR",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontFamily: "Poppins-Bold",
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0, left: 4),
                      child: Row(
                        children: [
                          SvgPicture.asset("assets/Icons/Location.svg",
                              height: 17.h, width: 14.w),
                          SizedBox(
                            width: 5.w,
                          ),
                          Text(
                            "Jeddah, Saudi Arabia",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontFamily: "Poppins-Bold",
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Spacer(),
                          Text(
                            "06 Aug",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontFamily: "Poppins-Bold",
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 20.0,
                      ),
                      child: Divider(
                        thickness: 2,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Row(
                        children: [
                          Text(
                            "Details",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 9.0),
                      child: Row(
                        children: [
                          Text(
                            "Brand",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(
                            width: 86.w,
                          ),
                          Text(
                            "Apple",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10.0,
                      ),
                      child: Row(
                        children: [
                          Text(
                            "Model",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(
                            width: 83.w,
                          ),
                          Text(
                            "Apple air pods 2nd Generation",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 20.0,
                      ),
                      child: Divider(thickness: 2),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Text(
                        "Description",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontFamily: "Poppins-SemiBold",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        "New Condition",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontFamily: "Poppins-Regular",
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                      child: Divider(
                        thickness: 2,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        "Customer review",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontFamily: "Poppins-SemiBold",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0),
                      child: Row(
                        children: [
                          RatingBar.builder(
                            initialRating: 3,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemSize: 20,
                            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          ),
                          SizedBox(width: 10),
                          Text(
                            "3 out of 5",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Spacer(),
                          Icon(Icons.arrow_forward_ios_outlined, size: 15),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Text(
                        "1000 global ratings",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontFamily: "Poppins-Regular",
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0, bottom: 18),
                      child: InkWell(
                        onTap: (BottomSheet),
                        child: Container(
                          height: 57.h,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Color(0xff0AD188),
                            border: Border.all(color: Color(0xff0AD188)),
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Center(
                            child: Text(
                              "Boost Item",
                              style: TextStyle(
                                color: Color.fromARGB(255, 255, 255, 255),
                                fontSize: 16,
                                fontFamily: "Poppins-Regular",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
