// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myitems/boostitem.dart';

class MyItems extends StatefulWidget {
  const MyItems({Key? key}) : super(key: key);

  @override
  State<MyItems> createState() => _MyItemsState();
}

class _MyItemsState extends State<MyItems> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 9, left: 15),
                    child: Row(
                      children: [
                        InkWell(
                          // close keyboard on outside input tap
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset(
                            "assets/Icons/back.svg",
                            height: 21,
                            width:
                                12, //just like you define in pubspec.yaml file
                          ),
                        ),
                        const Spacer(),
                        const Text(
                          "My Items",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        ),
                        const Spacer(),
                        const SizedBox(
                          height: 21,
                          width: 12,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 17.0, right: 15, left: 15),
                    child: Row(
                      children: [
                        Container(
                          height: 40.h,
                          decoration: BoxDecoration(
                            color: Color(0xff0AD188),
                            border: Border.all(color: Color(0xff0AD188)),
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, bottom: 8, left: 10, right: 10),
                              child: Text(
                                "On Sell",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                          height: 40.h,
                          decoration: BoxDecoration(
                            border: Border.all(color: Color(0xff0AD188)),
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, bottom: 8, left: 15, right: 15),
                              child: Text(
                                "Sold",
                                style: TextStyle(
                                  color: Color(0xff0AD188),
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                          height: 40.h,
                          decoration: BoxDecoration(
                            border: Border.all(color: Color(0xff0AD188)),
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, bottom: 8, left: 15, right: 15),
                              child: Text(
                                "Buy",
                                style: TextStyle(
                                  color: Color(0xff0AD188),
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                          height: 40.h,
                          decoration: BoxDecoration(
                            border: Border.all(color: Color(0xff0AD188)),
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, bottom: 8, left: 10, right: 10),
                              child: Text(
                                "Booked Items",
                                style: TextStyle(
                                  color: Color(0xff0AD188),
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10.0, right: 15, left: 15),
                    child: Container(
                      // height: 470.h,
                      // color: Colors.green,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => BoostItem()),
                          );
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SingleChildScrollView(
                              child: Container(
                                // height: 470.h,
                                // height: MediaQuery.of(context).size.height * 0.58,
                                // decoration: BoxDecoration(color: Colors.white),
                                child: GridView.builder(
                                    shrinkWrap: true,
                                    physics: ScrollPhysics(),
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      childAspectRatio: 0.90.r,
                                      mainAxisSpacing: 5.r,
                                      crossAxisSpacing: 15.0.r,
                                    ),
                                    itemCount: 10,
                                    itemBuilder: (BuildContext ctx, index) {
                                      return Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: 160.w,
                                            decoration: BoxDecoration(
                                              color: Color.fromARGB(
                                                  198, 255, 255, 255),
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              boxShadow: const [
                                                BoxShadow(
                                                    color: Color.fromARGB(
                                                        255, 221, 216, 216),
                                                    blurRadius: 5,
                                                    spreadRadius: 1,
                                                    offset: Offset(4, 4)),
                                              ],
                                            ),
                                            child: Wrap(
                                              children: [
                                                Stack(
                                                  children: [
                                                    Image.asset(
                                                      "assets/Images/airpods.png",
                                                    ),
                                                    Column(
                                                      children: [
                                                        Align(
                                                          alignment: Alignment
                                                              .topRight,
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 11,
                                                                    right: 12),
                                                            child: Container(
                                                                width: 20.w,
                                                                height: 20.h,
                                                                decoration: BoxDecoration(
                                                                    shape: BoxShape
                                                                        .circle,
                                                                    color: Color
                                                                        .fromARGB(
                                                                            255,
                                                                            216,
                                                                            206,
                                                                            206)),
                                                                child: Center(
                                                                  child: SvgPicture.asset(
                                                                      "assets/Icons/heart.svg",
                                                                      height:
                                                                          11.h,
                                                                      width:
                                                                          12.w),
                                                                )),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 11),
                                                      child: Container(
                                                        height: 20.h,
                                                        width: 65.w,
                                                        decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.only(
                                                                topRight: Radius
                                                                    .circular(
                                                                        10.0),
                                                                bottomRight: Radius
                                                                    .circular(
                                                                        10.0)),
                                                            color:
                                                                Color.fromARGB(
                                                                    239,
                                                                    66,
                                                                    190,
                                                                    138)),
                                                        child: Center(
                                                          child: Text(
                                                              "FEATURED",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 10,
                                                                fontFamily:
                                                                    "Poppins-Medium",
                                                              )),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Container(
                                                  // height: 70.h,
                                                  // color: Colors.grey,
                                                  child: Column(
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                top: 5,
                                                                left: 8,
                                                                right: 5),
                                                        child: Row(
                                                          children: [
                                                            Text(
                                                              "7,000 SAR",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 14,
                                                                fontFamily:
                                                                    "Poppins-SemiBold",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ),
                                                            ),
                                                            Spacer(),
                                                            Container(
                                                              height: 18.h,
                                                              width: 37.w,
                                                              decoration: BoxDecoration(
                                                                  color: Color
                                                                      .fromARGB(
                                                                          255,
                                                                          221,
                                                                          30,
                                                                          30),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              8)),
                                                              child: Center(
                                                                child: Text(
                                                                  "USED",
                                                                  style:
                                                                      TextStyle(
                                                                    color: Color
                                                                        .fromARGB(
                                                                            255,
                                                                            248,
                                                                            246,
                                                                            246),
                                                                    fontSize:
                                                                        10,
                                                                    fontFamily:
                                                                        "Poppins-Regular",
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                top: 1,
                                                                left: 8,
                                                                right: 5),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              "Apple airpods",
                                                              textAlign:
                                                                  TextAlign
                                                                      .left,
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 12,
                                                                fontFamily:
                                                                    "Poppins-Regular",
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                top: 2,
                                                                left: 8,
                                                                right: 5,
                                                                bottom: 9),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              "Jeddah, Saudi Arabia",
                                                              textAlign:
                                                                  TextAlign
                                                                      .left,
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 10,
                                                                fontFamily:
                                                                    "Poppins-Regular",
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      // Container(
                                                      //     height: 2.h,
                                                      //     color: Colors.grey),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      );
                                    }),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
