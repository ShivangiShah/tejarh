// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, unnecessary_new, avoid_unnecessary_containers, unnecessary_this

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TrackOrder extends StatefulWidget {
  const TrackOrder({Key? key}) : super(key: key);

  @override
  State<TrackOrder> createState() => _TrackOrderState();
}

class _TrackOrderState extends State<TrackOrder> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
            body: Column(children: [
      Padding(
        padding: EdgeInsets.only(top: 9, left: 15, right: 15),
        child: Row(
          children: [
            InkWell(
              // close keyboard on outside input tap
              onTap: () {
                Navigator.of(context).pop();
              },
              child: SvgPicture.asset(
                "assets/Icons/back.svg",
                height: 21,
                width: 12, //just like you define in pubspec.yaml file
              ),
            ),
            const Spacer(),
            const Text(
              "Track Order",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontFamily: "Poppins-SemiBold",
                  fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
            const Spacer(),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(top: 56.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 26.0, top: 7),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 30.h,
                      width: 30.w,
                      child: Image.asset("assets/Images/track.png")),
                  SizedBox(
                    width: 30.w,
                    height: 70.h,
                    child: Center(
                      child: Container(
                        width: 1.w,
                        height: 70.h,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Container(
                      height: 30.h,
                      width: 30.w,
                      child: Image.asset("assets/Images/track.png")),
                  SizedBox(
                    width: 30.w,
                    height: 70.h,
                    child: Center(
                      child: Container(
                        width: 1.w,
                        height: 70.h,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Container(
                      height: 30.h,
                      width: 30.w,
                      child: Image.asset("assets/Images/track.png")),
                  SizedBox(
                    width: 30.w,
                    height: 70.h,
                    child: Center(
                      child: Container(
                        width: 1.w,
                        height: 70.h,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Container(
                      height: 30.h,
                      width: 30.w,
                      child: Image.asset("assets/Images/white.png")),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 22.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Order placed",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontFamily: "Poppins-Medium",
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Text(
                      "18th Aug",
                      style: TextStyle(
                        color: Color.fromARGB(255, 192, 191, 191),
                        fontSize: 12,
                        fontFamily: "Poppins-Medium",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 50.0),
                    child: Text(
                      "Item Shipped to delivery center",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontFamily: "Poppins-Medium",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Text(
                      "19th Aug",
                      style: TextStyle(
                        color: Color.fromARGB(255, 192, 191, 191),
                        fontSize: 12,
                        fontFamily: "Poppins-Medium",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 50.0),
                    child: Text(
                      "Item out for delivery",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontFamily: "Poppins-Medium",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Text(
                      "20th Aug",
                      style: TextStyle(
                        color: Color.fromARGB(255, 192, 191, 191),
                        fontSize: 12,
                        fontFamily: "Poppins-Medium",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 50.0),
                    child: Text(
                      "Delivered",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontFamily: "Poppins-Medium",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Text(
                      "20th Aug",
                      style: TextStyle(
                        color: Color.fromARGB(255, 192, 191, 191),
                        fontSize: 12,
                        fontFamily: "Poppins-Medium",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ])));
  }
}
