// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, prefer_final_fields, unused_field, constant_identifier_names, unused_element, dead_code, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:group_radio_button/group_radio_button.dart';

class ReturnItem extends StatefulWidget {
  const ReturnItem({Key? key}) : super(key: key);

  @override
  State<ReturnItem> createState() => _ReturnItemState();
}

class _ReturnItemState extends State<ReturnItem> {
  int _stackIndex = 0;

  // String _singleValue = "Text alignment right";
  String _verticalGroupValue = "Exchange";

  List<String> _status = ["Exchange", "Refund"];
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Color.fromARGB(255, 0, 0, 0);
      }
      return Color.fromARGB(255, 0, 0, 0);
    }

    return SafeArea(
        child: Scaffold(
            body: Column(
      children: [
        Expanded(
          child: Padding(
              padding: const EdgeInsets.only(left: 15.0, right: 15),
              child: Column(children: [
                Padding(
                  padding: EdgeInsets.only(top: 9),
                  child: Row(
                    children: [
                      InkWell(
                        // close keyboard on outside input tap
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: SvgPicture.asset(
                          "assets/Icons/back.svg",
                          height: 21,
                          width: 12, //just like you define in pubspec.yaml file
                        ),
                      ),
                      Spacer(),
                      Text(
                        "Return Item",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontFamily: "Poppins-SemiBold",
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                      Spacer(),
                      SizedBox(
                        height: 21,
                        width: 12,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        "assets/Images/airpods.png",
                        height: 100.h,
                        // width: 146.w,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Column(
                          children: [
                            Text(
                              "Apple airpods",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w400),
                              // textAlign: TextAlign.left,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10.0),
                              child: Text(
                                "7,000 SAR",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontFamily: "Poppins-SemiBold",
                                    fontWeight: FontWeight.w600),
                                // textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Divider(
                    thickness: 2,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 23.0),
                  child: IndexedStack(index: _stackIndex, children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RadioGroup<String>.builder(
                          direction: Axis.horizontal,
                          groupValue: _verticalGroupValue,
                          horizontalAlignment: MainAxisAlignment.spaceAround,
                          onChanged: (value) => setState(() {
                            _verticalGroupValue = value!;
                          }),
                          items: _status,
                          textStyle:
                              TextStyle(fontSize: 16, color: Colors.black),
                          itemBuilder: (item) => RadioButtonBuilder(
                            item,
                          ),
                        ),
                      ],
                    ),
                  ]),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Select a reason for return',
                      hintStyle: TextStyle(color: Colors.grey),
                      suffixIcon: Align(
                        widthFactor: 1.0,
                        heightFactor: 1.0,
                        child: Icon(
                          Icons.expand_more_sharp,
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 15),
                  child: TextField(
                    maxLines: 4,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Describe your items',
                      hintStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Divider(
                    thickness: 2,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Row(
                    children: [
                      Text(
                        "Pickup from",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontFamily: "Poppins-SemiBold",
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SvgPicture.asset("assets/Icons/Location.svg",
                          width: 21.w, height: 26.h),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Text(
                          "P.O Box 401247, \nDubai",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w400),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Spacer(),
                      Container(
                        height: 34.h,
                        width: 86.w,
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xff0AD188)),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Center(
                          child: Text(
                            "Change",
                            style: TextStyle(
                                color: Color(0xff0AD188),
                                fontSize: 16,
                                fontFamily: "Poppins-Medium",
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Divider(
                    thickness: 2,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Checkbox(
                      checkColor: Colors.white,
                      fillColor: MaterialStateProperty.resolveWith(getColor),
                      value: isChecked,
                      onChanged: (bool? value) {
                        setState(() {
                          isChecked = value!;
                        });
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 9.0),
                      child: Text(
                        "I agree to return the item(s) back in original \ncondition.",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: "Poppins-Regular",
                            fontWeight: FontWeight.w400),
                        // textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ])),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 15.0, right: 15, bottom: 5),
          child: Container(
            height: 57.h,
            width: double.infinity,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7)),
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => _________()),
                // );
              },
              color: const Color.fromARGB(239, 66, 190, 138),
              child: const Text("Submit",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: "Poppins-Regular")),
            ),
          ),
        ),
      ],
    )));
  }
}
