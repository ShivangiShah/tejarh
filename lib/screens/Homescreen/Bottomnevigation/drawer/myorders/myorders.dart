// ignore_for_file: prefer_const_constructors, deprecated_member_use, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/bottomnavigation.dart';

class MyOrders extends StatefulWidget {
  const MyOrders({Key? key}) : super(key: key);

  @override
  State<MyOrders> createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrders> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, right: 15),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                            top: 9,
                          ),
                          child: Row(
                            children: [
                              InkWell(
                                // close keyboard on outside input tap
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                                child: SvgPicture.asset(
                                  "assets/Icons/back.svg",
                                  height: 21.h,
                                  width: 12
                                      .w, //just like you define in pubspec.yaml file
                                ),
                              ),
                              const Spacer(),
                              const Text(
                                "My Orders",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontFamily: "Poppins-SemiBold",
                                    fontWeight: FontWeight.w700),
                                textAlign: TextAlign.center,
                              ),
                              const Spacer(),
                              const SizedBox(
                                height: 21,
                                width: 12,
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 13.5),
                          child: Row(
                            children: [
                              Container(
                                width: 285.w,
                                child: TextField(
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(15),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(28)),
                                    hintText: 'Search',
                                    hintStyle: TextStyle(color: Colors.grey),
                                    prefixIcon: Align(
                                      widthFactor: 1,
                                      heightFactor: 1,
                                      child: Icon(Icons.search_outlined),
                                    ),
                                  ),
                                ),
                              ),
                              Spacer(),
                              Container(
                                  height: 50.h,
                                  width: 50.w,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Color.fromARGB(239, 66, 190, 138)),
                                  child: Image.asset(
                                      "assets/Icons/settings.png",
                                      height: 25.h,
                                      width: 25.w)),
                            ],
                          ),
                        ),
                        Container(
                            height: MediaQuery.of(context).size.height * 0.82,
                            child: ListView.builder(
                                scrollDirection: Axis.vertical,
                                itemCount: 15,
                                shrinkWrap: true,
                                physics: ScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return Column(
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 20.0),
                                            child: Image.asset(
                                              "assets/Images/airpods.png",
                                              width: 131.w,
                                              height: 96.h,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 27.0, left: 10),
                                            child: Container(
                                              height: 96.h,
                                              // color: Colors.amber,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceAround,
                                                children: [
                                                  Text(
                                                    "Order Confirm",
                                                    style: TextStyle(
                                                      color: Color(0xffEC6600),
                                                      fontSize: 16,
                                                      fontFamily:
                                                          "Poppins-SemiBold",
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Text(
                                                    "Apple airpods",
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 14,
                                                      fontFamily:
                                                          "Poppins-Regular",
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                  Text(
                                                    "7,000 SAR",
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 16,
                                                      fontFamily:
                                                          "Poppins-SemiBold",
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 15.0),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Container(
                                                height: 57.h,
                                                // width: double.infinity,
                                                child: RaisedButton(
                                                  shape: RoundedRectangleBorder(
                                                    side: BorderSide(
                                                      color:
                                                          const Color.fromARGB(
                                                              239,
                                                              66,
                                                              190,
                                                              138),
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            7),
                                                  ),
                                                  onPressed: () {
                                                    // Navigator.push(
                                                    //   context,
                                                    //   MaterialPageRoute(
                                                    //       builder: (context) => PaymentMethod()),
                                                    // );
                                                  },
                                                  color: Colors.white,
                                                  child: const Text(
                                                      "Track Order",
                                                      style: TextStyle(
                                                          color:
                                                              Color(0xff0AD188),
                                                          fontSize: 16,
                                                          fontFamily:
                                                              "Poppins-Regular")),
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: 10.w),
                                            Expanded(
                                              child: Container(
                                                height: 57.h,
                                                // width: double.infinity,
                                                child: RaisedButton(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              7)),
                                                  onPressed: () {
                                                    // Navigator.push(
                                                    //   context,
                                                    //   MaterialPageRoute(
                                                    //       builder: (context) => PaymentMethod()),
                                                    // );
                                                  },
                                                  color: const Color.fromARGB(
                                                      239, 66, 190, 138),
                                                  child: const Text("Cancel",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 16,
                                                          fontFamily:
                                                              "Poppins-Regular")),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 21.0, bottom: 20),
                                        child: Divider(thickness: 2),
                                      ),
                                    ],
                                  );
                                }))
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: BottomMenu(
              selectedIndex: 3,
            ),
          )
        ],
      ),
    ));
  }
}
