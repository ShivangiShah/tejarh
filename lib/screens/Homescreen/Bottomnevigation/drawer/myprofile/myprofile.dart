// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myprofile/editprofile.dart';

class MyProfile extends StatefulWidget {
  const MyProfile({Key? key}) : super(key: key);

  @override
  State<MyProfile> createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
    //   statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
    //   statusBarIconBrightness: Brightness.dark, // status bar icons' color
    //   systemNavigationBarIconBrightness:
    //       Brightness.dark, //navigation bar icons' color
    // ));

    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(children: [
          Expanded(
              child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 220.h,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        height: 220.h,
                        alignment: Alignment.topCenter,
                        child: Container(
                          height: 180.h,
                          width: double.infinity,
                          child: GridTile(
                            child: Image.asset(
                              "assets/Images/seller_bg.png",
                              fit: BoxFit.fill,
                            ),
                            header: Padding(
                              padding:
                                  EdgeInsets.only(top: 53, left: 15, right: 15),
                              child: Row(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: SvgPicture.asset(
                                      "assets/Icons/back.svg",
                                      color: Color(0xff262E3A),
                                      height: 21,
                                      width: 12,
                                    ),
                                  ),
                                  const Spacer(),
                                  const Text(
                                    "Profile",
                                    style: TextStyle(
                                        color: Color(0xff262E3A),
                                        fontSize: 20,
                                        fontFamily: "Poppins-SemiBold",
                                        fontWeight: FontWeight.w700),
                                    textAlign: TextAlign.center,
                                  ),
                                  const Spacer(),
                                  InkWell(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  EditProfile()),
                                        );
                                      },
                                      child: Icon(Icons.edit)),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          height: 112.h,
                          width: 112.w,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                          child: Stack(
                            children: [
                              Center(
                                child: Container(
                                  height: 112.h,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                  ),
                                  child: Image.asset(
                                      "assets/Images/item_avtar.png",
                                      height: 112.h),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    right: 15.0, bottom: 3),
                                child: Align(
                                  alignment: Alignment.bottomRight,
                                  child: Container(
                                    width: 20.w,
                                    height: 20.h,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Color(0xff0AD188)),
                                        shape: BoxShape.circle,
                                        color: Color(0xff0AD188)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "John Doe",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontFamily: "Poppins-SemiBold",
                            fontWeight: FontWeight.w600),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 9.0),
                        child: Image.asset(
                          "assets/Images/award.png",
                          height: 27.h,
                          width: 27.w,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 3.0),
                  child: Text(
                    "Member since jan 2020",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                        fontFamily: "Poppins-Regular",
                        fontWeight: FontWeight.w400),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 3),
                  child: RatingBar.builder(
                    initialRating: 3,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemSize: 20,
                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset("assets/Icons/Location.svg"),
                      SizedBox(width: 10),
                      Text(
                        "Riyadh, Saudi Arabia",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: "Poppins-Regular",
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 22),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      IntrinsicHeight(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                              width: 60.w,
                              child: Column(
                                children: [
                                  Text(
                                    "10",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 30,
                                        fontFamily: "Poppins-SemiBold",
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    "Bought",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontFamily: "Poppins-Regular",
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ),
                            ),
                            VerticalDivider(
                              color: Colors.grey,
                              thickness: 2,
                            ),
                            Container(
                              width: 60.w,
                              child: Column(
                                children: [
                                  Text(
                                    "20",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 30,
                                        fontFamily: "Poppins-SemiBold",
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    "Sold",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontFamily: "Poppins-Regular",
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ),
                            ),
                            VerticalDivider(
                              color: Colors.grey,
                              thickness: 2,
                            ),
                            Container(
                              width: 60.w,
                              child: Column(
                                children: [
                                  Text(
                                    "5",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 30,
                                        fontFamily: "Poppins-SemiBold",
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    "Followers",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontFamily: "Poppins-Regular",
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ),
                            ),
                            VerticalDivider(
                              color: Colors.grey,
                              thickness: 2,
                            ),
                            Container(
                              width: 60.w,
                              child: Column(
                                children: [
                                  Text(
                                    "20",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 30,
                                        fontFamily: "Poppins-SemiBold",
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    "Following",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontFamily: "Poppins-Regular",
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  // width: double.infinity,
                  // color: Colors.amber,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 24.0, right: 15, left: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          children: [
                            Image.asset("assets/Icons/mystory.png",
                                height: 54.h, width: 54.w),
                            SizedBox(height: 7.h),
                            Text(
                              "My Story",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontFamily: "Poppins-Regular",
                              ),
                            ),
                          ],
                        ),
                        Container(
                          height: 82.h,
                          // width: double.infinity,
                          // height: 100.h,
                          // color: Colors.blue,
                          width: MediaQuery.of(context).size.width - 90.w,
                          // height: MediaQuery.of(context).size.height * 0.150.w,
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: 10,
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                    left: 8,
                                  ),
                                  child: Container(
                                    // color: Colors.green,
                                    height: 82.h,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          height: 56.h,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Color(0xff0AD188),
                                                width: 2.0),
                                            shape: BoxShape.circle,
                                            color: Colors.white,
                                          ),
                                          child: Center(
                                            child: Image.asset(
                                                "assets/Icons/Jessy314.png",
                                                height: 54.h,
                                                width: 54.w),
                                          ),
                                        ),
                                        SizedBox(height: 10.h),
                                        Text(
                                          "Jessy314",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                            fontFamily: "Poppins-Regular",
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20, left: 15.0, right: 15),
                  child: Divider(thickness: 2),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 15.0, right: 15, top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Verified your account",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontFamily: "Poppins-SemiBold",
                            fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Image.asset(
                            "assets/Icons/email.png",
                            height: 30.h,
                          ),
                          SizedBox(height: 5.h),
                          Text(
                            "  Email \n Verified",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontFamily: "Poppins-Regular",
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/Icons/call.png",
                            height: 30.h,
                          ),
                          SizedBox(height: 5.h),
                          Text(
                            "  Phone \n Verified",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontFamily: "Poppins-Regular",
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/Icons/id-card.png",
                            height: 30.h,
                          ),
                          SizedBox(height: 5.h),
                          Text(
                            "Government \n    Verified",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontFamily: "Poppins-Regular",
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20, left: 15.0, right: 15),
                  child: Divider(thickness: 2),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 15.0, right: 15, top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Verified seller badge",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontFamily: "Poppins-SemiBold",
                            fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 15.0, left: 15, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 15.0),
                        child: Column(
                          children: [
                            Image.asset(
                              "assets/Icons/calendar.png",
                              height: 30.h,
                            ),
                            SizedBox(height: 5.h),
                            Text(
                              "Member Since \n 1 year",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontFamily: "Poppins-Regular",
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/Icons/truck.png",
                            height: 30.h,
                          ),
                          SizedBox(height: 5.h),
                          Text(
                            "Quick Shipper \n     ",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontFamily: "Poppins-Regular",
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 15.0),
                        child: Column(
                          children: [
                            Image.asset(
                              "assets/Icons/security.png",
                              height: 30.h,
                            ),
                            SizedBox(height: 5.h),
                            Text(
                              "Reliable \n    ",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontFamily: "Poppins-Regular",
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ))
        ]));
  }
}