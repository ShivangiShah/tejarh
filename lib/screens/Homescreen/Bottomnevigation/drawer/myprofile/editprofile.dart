// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
// import 'package:tejarh/screens/Homescreen/Bottomnevigation/Profile/changepassword.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myprofile/changepassword.dart';


class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Column(children: [
            Expanded(
                child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 9, left: 15),
                    child: Row(
                      children: [
                        InkWell(
                          // close keyboard on outside input tap
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset(
                            "assets/Icons/back.svg",
                            height: 21,
                            width:
                                12, //just like you define in pubspec.yaml file
                          ),
                        ),
                        const Spacer(),
                        const Text(
                          "Edit Profile",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        ),
                        const Spacer(),
                        const SizedBox(
                          height: 21,
                          width: 12,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 23.0),
                    child: Container(
                      height: 114.h,
                      width: 114.w,
                      // color: Colors.black,
                      child: Stack(
                        children: [
                          Center(
                            child: Container(
                              // color: Colors.amber,
                              height: 114.h,
                              child: Image.asset("assets/Images/item_avtar.png",
                                  height: 114.h),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                                width: 26.w,
                                height: 26.h,
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xff0AD188)),
                                    shape: BoxShape.circle,
                                    color: Colors.white),
                                child: Center(
                                  child:
                                      Icon(Icons.camera_alt_rounded, size: 14),
                                )),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 15, left: 15),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 18.0),
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'UserName',
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Email',
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Gender',
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Pick your birth date',
                              suffixIcon: Align(
                                widthFactor: 1.0,
                                heightFactor: 1.0,
                                child: Icon(
                                  Icons.calendar_today_sharp,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Mobile number',
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Location',
                              suffixIcon: Padding(
                                padding:
                                    const EdgeInsets.only(right: 20.0, top: 15),
                                child: Container(
                                  child: Text("Change",
                                      style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 49, 207, 160),
                                          fontSize: 16,
                                          fontFamily: "Poppins-Regular")),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChangePassword()),
                              );
                            },
                            child: Container(
                              height: 57.h,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                border: Border.all(color: Color(0xff0AD188)),
                                borderRadius: BorderRadius.circular(7),
                              ),
                              child: Center(
                                child: Text(
                                  "Change Password",
                                  style: TextStyle(
                                    color: Color(0xff0AD188),
                                    fontSize: 16,
                                    fontFamily: "Poppins-Regular",
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                          child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              height: 57,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(7)),
                                onPressed: () {
                                  // Navigator.push(
                                  //   context,
                                  //   MaterialPageRoute(builder: (context) => HomeScreen()),
                                  // );
                                },
                                color: const Color.fromARGB(239, 66, 190, 138),
                                child: const Text("Save",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontFamily: "Poppins-Regular")),
                              )),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ))
          ])),
    );
  }
}
