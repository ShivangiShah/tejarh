// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, dead_code

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/payment/addmoney.dart';


class Wallet extends StatefulWidget {
  const Wallet({Key? key}) : super(key: key);

  @override
  State<Wallet> createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
            body: Column(children: [
      Expanded(
          child: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 9.0, left: 15, right: 15),
              child: Row(
                children: [
                  InkWell(
                    // close keyboard on outside input tap
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: SvgPicture.asset(
                      "assets/Icons/back.svg",
                      height: 21,
                      width: 12, //just like you define in pubspec.yaml file
                    ),
                  ),
                  Spacer(),
                  const Text(
                    "Wallet",
                    style: TextStyle(
                        color: Color(0xff262E3A),
                        fontSize: 20,
                        fontFamily: "Poppins-SemiBold",
                        fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                  const Spacer(),
                  SizedBox(
                    width: 12,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 15, right: 15),
              child: Container(
                width: double.infinity,
                height: 111.h,
                decoration: BoxDecoration(
                  color: Color(0xff0AD188),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 14.0, left: 24),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Current Balance",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w400),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Text(
                                "\$1340",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 37,
                                    fontFamily: "Poppins-Regular",
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AddMoney()),
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(right: 24.0),
                        child: SvgPicture.asset("assets/Icons/add.svg"),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 15, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Last activity",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontFamily: "Poppins-Regular",
                        fontWeight: FontWeight.w400),
                  ),
                  Text(
                    "See All",
                    style: TextStyle(
                        color: Color.fromARGB(255, 63, 226, 177),
                        fontSize: 16,
                        fontFamily: "Poppins-Regular",
                        fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Container(
                width: double.infinity,
                height: 30.h,
                color: Color.fromARGB(255, 230, 231, 231),
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 5),
                  child: Text(
                    "August 2021",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontFamily: "Poppins-Regular",
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ),
            Container(
                height: MediaQuery.of(context).size.height * 0.90,
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: 15,
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 19.0, left: 15, right: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                              color: Colors.grey,
                                            )),
                                        child: Padding(
                                          padding: const EdgeInsets.all(12.0),
                                          child: SvgPicture.asset(
                                              "assets/Icons/arrow-red.svg"),
                                        )),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(left: 10.0),
                                      child: Column(
                                        children: [
                                          Text(
                                            "Blick-Nolan",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontFamily: "Poppins-Medium",
                                                fontWeight: FontWeight.w400),
                                          ),
                                          Text(
                                            "10 Jul, 12:13 PM",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 11,
                                                fontFamily: "Poppins-Regular",
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Text(
                                  "-\$250",
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontSize: 18,
                                      fontFamily: "Poppins-Medium",
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 60, right: 15),
                            child: Divider(thickness: 2),
                          )
                        ],
                      );
                    })),
          ],
        ),
      ))
    ])));
  }
}
