// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace, prefer_const_literals_to_create_immutables, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/bottomnavigation.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myitems/myitems.dart';

class PostAnItem extends StatefulWidget {
  const PostAnItem({Key? key}) : super(key: key);

  @override
  State<PostAnItem> createState() => _PostAnItemState();
}

class _PostAnItemState extends State<PostAnItem> {
  int _stackIndex = 0;

  // String _singleValue = "Text alignment right";
  String _verticalGroupValue = "The buyer";

  List<String> _status = ["The buyer", "I'll pay", "Split"];
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Color.fromARGB(255, 0, 0, 0);
      }
      return Color.fromARGB(255, 0, 0, 0);
    }

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 9),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text(
                              "Post an item",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.w700),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Row(
                          children: [
                            const Text(
                              "Upload Item picture.",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.w700),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 13.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              // onTap: () {
                              //   loadAssets(1);
                              // },
                              focusColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset("assets/Icons/photo.svg"),
                                    Text(
                                      "*Required",
                                      style: TextStyle(
                                          color: Color(0xffC5C5C5),
                                          fontSize: 14,
                                          fontFamily: "Poppins-Regular",
                                          fontWeight: FontWeight.w400),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                width: MediaQuery.of(context).size.width / 3.7,
                                height: MediaQuery.of(context).size.width / 3.2,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    border: Border.all(
                                        width: 2,
                                        color:
                                            Color.fromRGBO(235, 235, 235, 1))),
                              ),
                            ),
                            InkWell(
                              // onTap: () {
                              //   loadAssets(1);
                              // },
                              focusColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset("assets/Icons/photo.svg"),
                                    Text(
                                      "Image 2",
                                      style: TextStyle(
                                          color: Color(0xffC5C5C5),
                                          fontSize: 14,
                                          fontFamily: "Poppins-Regular",
                                          fontWeight: FontWeight.w400),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                width: MediaQuery.of(context).size.width / 3.7,
                                height: MediaQuery.of(context).size.width / 3.2,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    border: Border.all(
                                        width: 2,
                                        color:
                                            Color.fromRGBO(235, 235, 235, 1))),
                              ),
                            ),
                            InkWell(
                              // onTap: () {
                              //   loadAssets(1);
                              // },
                              focusColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset("assets/Icons/photo.svg"),
                                    Text(
                                      "Image 3",
                                      style: TextStyle(
                                          color: Color(0xffC5C5C5),
                                          fontSize: 14,
                                          fontFamily: "Poppins-Regular",
                                          fontWeight: FontWeight.w400),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                width: MediaQuery.of(context).size.width / 3.7,
                                height: MediaQuery.of(context).size.width / 3.2,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    border: Border.all(
                                        width: 2,
                                        color:
                                            Color.fromRGBO(235, 235, 235, 1))),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 11.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              // onTap: () {
                              //   loadAssets(1);
                              // },
                              focusColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset("assets/Icons/photo.svg"),
                                    Text(
                                      "Image 4",
                                      style: TextStyle(
                                          color: Color(0xffC5C5C5),
                                          fontSize: 14,
                                          fontFamily: "Poppins-Regular",
                                          fontWeight: FontWeight.w400),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                width: MediaQuery.of(context).size.width / 3.7,
                                height: MediaQuery.of(context).size.width / 3.2,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    border: Border.all(
                                        width: 2,
                                        color:
                                            Color.fromRGBO(235, 235, 235, 1))),
                              ),
                            ),
                            InkWell(
                              // onTap: () {
                              //   loadAssets(1);
                              // },
                              focusColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset("assets/Icons/photo.svg"),
                                    Text(
                                      "Image 5",
                                      style: TextStyle(
                                          color: Color(0xffC5C5C5),
                                          fontSize: 14,
                                          fontFamily: "Poppins-Regular",
                                          fontWeight: FontWeight.w400),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                width: MediaQuery.of(context).size.width / 3.7,
                                height: MediaQuery.of(context).size.width / 3.2,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    border: Border.all(
                                        width: 2,
                                        color:
                                            Color.fromRGBO(235, 235, 235, 1))),
                              ),
                            ),
                            InkWell(
                              // onTap: () {
                              //   loadAssets(1);
                              // },
                              focusColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset("assets/Icons/photo.svg"),
                                    Text(
                                      "Image 6",
                                      style: TextStyle(
                                          color: Color(0xffC5C5C5),
                                          fontSize: 14,
                                          fontFamily: "Poppins-Regular",
                                          fontWeight: FontWeight.w400),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                width: MediaQuery.of(context).size.width / 3.7,
                                height: MediaQuery.of(context).size.width / 3.2,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    border: Border.all(
                                        width: 2,
                                        color:
                                            Color.fromRGBO(235, 235, 235, 1))),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 13.0),
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Text(
                              "Description",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 13.0),
                        child: TextField(
                          decoration: InputDecoration(
                              fillColor: Color(0xffDFDFDF),
                              border: OutlineInputBorder(),
                              hintText: 'What are you selling?',
                              hintStyle: TextStyle(color: Color(0xffDFDFDF))),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: TextField(
                          maxLines: 3,
                          decoration: InputDecoration(
                              fillColor: Color(0xffDFDFDF),
                              border: OutlineInputBorder(),
                              hintText: 'Describe your items',
                              hintStyle: TextStyle(color: Color(0xffDFDFDF))),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Text(
                              "Details",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.w700),
                            ),
                            Spacer(),
                            const Text(
                              "*Fields add as per category",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 14,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Select category',
                            hintStyle: TextStyle(color: Colors.black),
                            suffixIcon: Align(
                              widthFactor: 1.0,
                              heightFactor: 1.0,
                              child: Icon(
                                Icons.expand_more_sharp,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Select Subcategorie',
                            hintStyle: TextStyle(color: Colors.black),
                            suffixIcon: Align(
                              widthFactor: 1.0,
                              heightFactor: 1.0,
                              child: Icon(
                                Icons.expand_more_sharp,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Select brand',
                            hintStyle: TextStyle(color: Colors.black),
                            suffixIcon: Align(
                              widthFactor: 1.0,
                              heightFactor: 1.0,
                              child: Icon(
                                Icons.expand_more_sharp,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Select condition',
                            hintStyle: TextStyle(color: Colors.black),
                            suffixIcon: Align(
                              widthFactor: 1.0,
                              heightFactor: 1.0,
                              child: Icon(
                                Icons.expand_more_sharp,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Enter Weight',
                            hintStyle: TextStyle(color: Colors.grey),
                          ),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Select Qty.',
                            hintStyle: TextStyle(color: Colors.black),
                            suffixIcon: Align(
                              widthFactor: 1.0,
                              heightFactor: 1.0,
                              child: Icon(
                                Icons.expand_more_sharp,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Text(
                              "Ship from",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Input zip code',
                            hintStyle: TextStyle(color: Colors.grey),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Text(
                              "Ship mode",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Ship by courier',
                            hintStyle: TextStyle(color: Colors.black),
                            suffixIcon: Align(
                              widthFactor: 1.0,
                              heightFactor: 1.0,
                              child: Icon(
                                Icons.expand_more_sharp,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Text(
                              "Who do you want to pay for shipping?",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0),
                        child:
                            IndexedStack(index: _stackIndex, children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RadioGroup<String>.builder(
                                direction: Axis.horizontal,
                                groupValue: _verticalGroupValue,
                                horizontalAlignment:
                                    MainAxisAlignment.spaceAround,
                                onChanged: (value) => setState(() {
                                  _verticalGroupValue = value!;
                                }),
                                items: _status,
                                textStyle: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                                itemBuilder: (item) => RadioButtonBuilder(
                                  item,
                                ),
                              ),
                            ],
                          ),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Text(
                              "Pricing",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: '1200 SAR',
                            hintStyle: TextStyle(color: Colors.black),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          children: [
                            Text(
                              "Commission Charge",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w400),
                              // textAlign: TextAlign.left,
                            ),
                            Spacer(),
                            Text(
                              "-100 SAR",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w500),
                              // textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          children: [
                            Text(
                              "Shipping Charge",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w400),
                              // textAlign: TextAlign.left,
                            ),
                            Spacer(),
                            Text(
                              "-100 SAR",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w500),
                              // textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          children: [
                            Text(
                              "Total",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w600),
                              // textAlign: TextAlign.left,
                            ),
                            Spacer(),
                            Text(
                              "1000 SAR",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w600),
                              // textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20, bottom: 20),
                        child: Container(
                          height: 57.h,
                          width: double.infinity,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(7)),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MyItems()),
                              );
                            },
                            color: const Color.fromARGB(239, 66, 190, 138),
                            child: const Text("Post your item",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontFamily: "Poppins-Regular")),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: BottomMenu(
                selectedIndex: 2,
              ),
            )
          ],
        ),
      ),
    );
  }
}
