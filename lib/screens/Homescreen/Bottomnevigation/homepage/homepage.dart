// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, prefer_const_constructors_in_immutables, sized_box_for_whitespace, dead_code, non_constant_identifier_names

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:tejarh/screens/Homescreen/Bottomnevigation/bottomnavigation.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/chat/chat.dart';

import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myorders/myorders.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/post%20an%20item/post_an_item.dart';
import 'package:tejarh/screens/Homescreen/Category/category.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

final List<String> imgList = [
  ("assets/Images/card1.png"),
  ("assets/Images/card2.png"),
  ("assets/Images/card1.png"),
  ("assets/Images/card2.png"),
];

class _HomePageState extends State<HomePage> {
  // int selectedIndex = 0;

  // List screens = [
  //   HomePage(),
  //   Chat(),
  //   PostAnItem(),
  //   MyOrders(),
  //   Account(),
  // ];

  // void onClicked(int index) {
  //   setState(() {
  //     selectedIndex = index;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    BottomSheet() {
      return showModalBottomSheet(
        context: context,
        barrierColor: Color.fromARGB(157, 146, 144, 144),
        backgroundColor: Colors.white,
        elevation: 20,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
        ),
        builder: (BuildContext context) {
          return Wrap(children: [
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Row(
                      children: [
                        Spacer(),
                        const Text(
                          'Filter',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        ),
                        Spacer(),
                        InkWell(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Icon(Icons.close)),
                      ],
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Divider(thickness: 2)),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.50,
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: 9,
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, bottom: 12),
                                child: Row(
                                  children: [
                                    Text("Most Viewed This Week"),
                                    Spacer(),
                                    Icon(Icons.arrow_forward_ios_outlined,
                                        size: 20),
                                  ],
                                ),
                              ),
                              Divider()
                            ],
                          );
                        }),
                  )
                ],
              ),
            ),
          ]);
        },
      );
    }

    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,

            // backgroundColor: Colors.green,
            body: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15.0, right: 15),
                      child: Column(children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 6),
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                "assets/Icons/Location.svg",
                                color: const Color.fromARGB(239, 66, 190, 138),
                                height: 17.01.h,
                                width: 14.14
                                    .w, //just like you define in pubspec.yaml file
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 5.33),
                                child: const Text(
                                  "P.O Box 401247, Dubai",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontFamily: "Poppins-Regular",
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: Icon(
                                  Icons.expand_more_sharp,
                                ),
                              ),
                              Spacer(),
                              SvgPicture.asset("assets/Icons/online.svg"),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 13.5),
                          child: TextField(
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(15),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(28)),
                              hintText: 'Search',
                              hintStyle: TextStyle(color: Colors.grey),
                              prefixIcon: Align(
                                widthFactor: 1,
                                heightFactor: 1,
                                child: Icon(Icons.search_outlined),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          // width: double.infinity,
                          // color: Colors.amber,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 12.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Column(
                                  children: [
                                    Image.asset("assets/Icons/mystory.png",
                                        height: 54.h, width: 54.w),
                                    SizedBox(height: 7.h),
                                    Text(
                                      "My Story",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontFamily: "Poppins-Regular",
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  height: 82.h,
                                  // width: double.infinity,
                                  // height: 100.h,
                                  // color: Colors.blue,
                                  width:
                                      MediaQuery.of(context).size.width - 90.w,
                                  // height: MediaQuery.of(context).size.height * 0.150.w,
                                  child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: 10,
                                      shrinkWrap: true,
                                      physics: ScrollPhysics(),
                                      itemBuilder: (context, index) {
                                        return Padding(
                                          padding: const EdgeInsets.only(
                                            left: 8,
                                          ),
                                          child: Container(
                                            // color: Colors.green,
                                            height: 82.h,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  height: 56.h,
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color:
                                                            Color(0xff0AD188),
                                                        width: 2.0),
                                                    shape: BoxShape.circle,
                                                    color: Colors.white,
                                                  ),
                                                  child: Center(
                                                    child: Image.asset(
                                                      "assets/Icons/Jessy314.png",
                                                      height: 54.h,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(height: 7.h),
                                                Text(
                                                  "Jessy314",
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12,
                                                    fontFamily:
                                                        "Poppins-Regular",
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      }),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.zero,
                          child: CarouselSlider(
                            options: CarouselOptions(
                              autoPlay: true,
                              aspectRatio: 2.0,
                              enlargeCenterPage: true,
                            ),
                            items: imageSliders,
                          ),
                        ),
                        Row(
                          children: [
                            Text(
                              "Category",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontFamily: "Poppins-SemiBold",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Spacer(),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const Category()),
                                );
                              },
                              child: Text(
                                "View All",
                                style: TextStyle(
                                  color: Color.fromARGB(239, 66, 190, 138),
                                  fontSize: 14,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8),
                          child: Container(
                            height: 100.h,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: 10,
                                shrinkWrap: true,
                                physics: ScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return Column(
                                    children: [
                                      Image.asset(
                                          "assets/Icons/electronics.png",
                                          height: 80.h,
                                          width: 80.w),
                                      SizedBox(height: 5.h),
                                      Text(
                                        "Electronics",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 10,
                                          fontFamily: "Poppins-Medium",
                                        ),
                                      ),
                                    ],
                                  );
                                }),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 22),
                          // ignore: prefer_const_literals_to_create_immutables
                          child: Row(
                            children: [
                              Text(
                                "Recommendation",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Spacer(),
                              InkWell(
                                onTap: (BottomSheet),
                                child: Container(
                                    height: 38.h,
                                    width: 38.w,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color:
                                            Color.fromARGB(239, 66, 190, 138)),
                                    child: Image.asset(
                                        "assets/Icons/settings.png",
                                        height: 10.h,
                                        width: 10.w)),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Container(
                            // height: 470.h,
                            // color: Colors.green,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SingleChildScrollView(
                                  child: Container(
                                    // height: 470.h,
                                    // height: MediaQuery.of(context).size.height * 0.58,
                                    // decoration: BoxDecoration(color: Colors.white),
                                    child: GridView.builder(
                                        shrinkWrap: true,
                                        physics: ScrollPhysics(),
                                        gridDelegate:
                                            SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2,
                                          childAspectRatio: 0.80.r,
                                          mainAxisSpacing: 10.r,
                                          crossAxisSpacing: 10.0.r,
                                        ),
                                        itemCount: 4,
                                        itemBuilder: (BuildContext ctx, index) {
                                          return Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Container(
                                                width: 160.w,
                                                decoration: BoxDecoration(
                                                  color: Color.fromARGB(
                                                      198, 255, 255, 255),
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  boxShadow: const [
                                                    BoxShadow(
                                                        color: Color.fromARGB(
                                                            255, 221, 216, 216),
                                                        blurRadius: 5,
                                                        spreadRadius: 1,
                                                        offset: Offset(4, 4)),
                                                  ],
                                                ),
                                                child: Wrap(
                                                  children: [
                                                    Stack(
                                                      children: [
                                                        Image.asset(
                                                          "assets/Images/airpods.png",
                                                        ),
                                                        Column(
                                                          children: [
                                                            Align(
                                                              alignment:
                                                                  Alignment
                                                                      .topRight,
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top: 11,
                                                                        right:
                                                                            12),
                                                                child:
                                                                    Container(
                                                                        width: 20
                                                                            .w,
                                                                        height: 20
                                                                            .h,
                                                                        decoration: BoxDecoration(
                                                                            shape: BoxShape
                                                                                .circle,
                                                                            color: Color.fromARGB(
                                                                                255,
                                                                                216,
                                                                                206,
                                                                                206)),
                                                                        child:
                                                                            Center(
                                                                          child: SvgPicture.asset(
                                                                              "assets/Icons/heart.svg",
                                                                              height: 11.h,
                                                                              width: 12.w),
                                                                        )),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  top: 11),
                                                          child: Container(
                                                            height: 20.h,
                                                            width: 65.w,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.only(
                                                                    topRight: Radius
                                                                        .circular(
                                                                            10.0),
                                                                    bottomRight:
                                                                        Radius.circular(
                                                                            10.0)),
                                                                color: Color
                                                                    .fromARGB(
                                                                        239,
                                                                        66,
                                                                        190,
                                                                        138)),
                                                            child: Center(
                                                              child: Text(
                                                                  "FEATURED",
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        10,
                                                                    fontFamily:
                                                                        "Poppins-Medium",
                                                                  )),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Container(
                                                      // height: 70.h,
                                                      // color: Colors.grey,
                                                      child: Column(
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 5,
                                                                    left: 8,
                                                                    right: 5),
                                                            child: Row(
                                                              children: [
                                                                Text(
                                                                  "7,000 SAR",
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .black,
                                                                    fontSize:
                                                                        14,
                                                                    fontFamily:
                                                                        "Poppins-SemiBold",
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                                Spacer(),
                                                                Container(
                                                                  height: 18.h,
                                                                  width: 37.w,
                                                                  decoration: BoxDecoration(
                                                                      color: Color.fromARGB(
                                                                          255,
                                                                          221,
                                                                          30,
                                                                          30),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              8)),
                                                                  child: Center(
                                                                    child: Text(
                                                                      "USED",
                                                                      style:
                                                                          TextStyle(
                                                                        color: Color.fromARGB(
                                                                            255,
                                                                            248,
                                                                            246,
                                                                            246),
                                                                        fontSize:
                                                                            10,
                                                                        fontFamily:
                                                                            "Poppins-Regular",
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 1,
                                                                    left: 8,
                                                                    right: 5),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Text(
                                                                  "Apple airpods",
                                                                  textAlign:
                                                                      TextAlign
                                                                          .left,
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .black,
                                                                    fontSize:
                                                                        12,
                                                                    fontFamily:
                                                                        "Poppins-Regular",
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 2,
                                                                    left: 8,
                                                                    right: 5),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Text(
                                                                  "Jeddah, Saudi Arabia",
                                                                  textAlign:
                                                                      TextAlign
                                                                          .left,
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .black,
                                                                    fontSize:
                                                                        10,
                                                                    fontFamily:
                                                                        "Poppins-Regular",
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          // Container(
                                                          //     height: 2.h,
                                                          //     color: Colors.grey),
                                                          Divider(),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 8,
                                                                    right: 5,
                                                                    bottom: 2),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Image.asset(
                                                                    "assets/Icons/location_icon.png",
                                                                    width: 16.w,
                                                                    height:
                                                                        17.h),
                                                                SizedBox(
                                                                  height: 17.h,
                                                                  width: 5.w,
                                                                ),
                                                                Text(
                                                                  "The Full Cart",
                                                                  textAlign:
                                                                      TextAlign
                                                                          .left,
                                                                  style:
                                                                      TextStyle(
                                                                    color: Color
                                                                        .fromARGB(
                                                                            239,
                                                                            66,
                                                                            190,
                                                                            138),
                                                                    fontSize:
                                                                        10,
                                                                    fontFamily:
                                                                        "Poppins-Regular",
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w700,
                                                                  ),
                                                                ),
                                                                Spacer(),
                                                                Container(
                                                                    height:
                                                                        10.h,
                                                                    width: 10.w,
                                                                    decoration: BoxDecoration(
                                                                        color: Color.fromARGB(
                                                                            239,
                                                                            66,
                                                                            190,
                                                                            138),
                                                                        shape: BoxShape
                                                                            .circle)),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          );
                                        }),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Divider(thickness: 3),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                "Used Items",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Spacer(),
                              InkWell(
                                onTap: () {
                                  // Navigator.push(
                                  //   context,
                                  //   MaterialPageRoute(
                                  //       builder: (context) => const MyOrders()),
                                  // );
                                },
                                child: Text(
                                  "View All",
                                  style: TextStyle(
                                    color: Color.fromARGB(239, 66, 190, 138),
                                    fontSize: 14,
                                    fontFamily: "Poppins-Regular",
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 18),
                          child: Container(
                            // height: 230.h,

                            height: 230.h,
                            alignment: Alignment.topCenter,

                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: 10,
                                shrinkWrap: true,
                                physics: ScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding: const EdgeInsets.only(left: 5.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: 160.w,
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(
                                                198, 255, 255, 255),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            boxShadow: const [
                                              BoxShadow(
                                                  color: Color.fromARGB(
                                                      255, 221, 216, 216),
                                                  blurRadius: 5,
                                                  spreadRadius: 1,
                                                  offset: Offset(4, 4)),
                                            ],
                                          ),
                                          child: Wrap(
                                            children: [
                                              Image.asset(
                                                "assets/Images/watch.png",
                                              ),
                                              Container(
                                                // height: 70.h,
                                                // color: Colors.grey,
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 5,
                                                              left: 8,
                                                              right: 5),
                                                      child: Row(
                                                        children: [
                                                          Text(
                                                            "7,000 SAR",
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Poppins-SemiBold",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          ),
                                                          Spacer(),
                                                          Container(
                                                            height: 18.h,
                                                            width: 37.w,
                                                            decoration: BoxDecoration(
                                                                color: Color
                                                                    .fromARGB(
                                                                        255,
                                                                        221,
                                                                        30,
                                                                        30),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            8)),
                                                            child: Center(
                                                              child: Text(
                                                                "USED",
                                                                style:
                                                                    TextStyle(
                                                                  color: Color
                                                                      .fromARGB(
                                                                          255,
                                                                          248,
                                                                          246,
                                                                          246),
                                                                  fontSize: 10,
                                                                  fontFamily:
                                                                      "Poppins-Regular",
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 1,
                                                              left: 8,
                                                              right: 5),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            "Apple airpods",
                                                            textAlign:
                                                                TextAlign.left,
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 12,
                                                              fontFamily:
                                                                  "Poppins-Regular",
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 2,
                                                              left: 8,
                                                              right: 5),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            "Jeddah, Saudi Arabia",
                                                            textAlign:
                                                                TextAlign.left,
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 10,
                                                              fontFamily:
                                                                  "Poppins-Regular",
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    // Container(
                                                    //     height: 2.h,
                                                    //     color: Colors.grey),
                                                    Divider(),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 8,
                                                              right: 5,
                                                              bottom: 2),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Image.asset(
                                                              "assets/Icons/location_icon.png",
                                                              width: 16.w,
                                                              height: 17.h),
                                                          SizedBox(
                                                            height: 17.h,
                                                            width: 5.w,
                                                          ),
                                                          Text(
                                                            "The Full Cart",
                                                            textAlign:
                                                                TextAlign.left,
                                                            style: TextStyle(
                                                              color: Color
                                                                  .fromARGB(
                                                                      239,
                                                                      66,
                                                                      190,
                                                                      138),
                                                              fontSize: 10,
                                                              fontFamily:
                                                                  "Poppins-Regular",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                            ),
                                                          ),
                                                          Spacer(),
                                                          Container(
                                                              height: 10.h,
                                                              width: 10.w,
                                                              decoration: BoxDecoration(
                                                                  color: Color
                                                                      .fromARGB(
                                                                          239,
                                                                          66,
                                                                          190,
                                                                          138),
                                                                  shape: BoxShape
                                                                      .circle)),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Divider(thickness: 3),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                "Unused Items",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Spacer(),
                              Text(
                                "View All",
                                style: TextStyle(
                                  color: Color.fromARGB(239, 66, 190, 138),
                                  fontSize: 14,
                                  fontFamily: "Poppins-Regular",
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 18),
                          child: Container(
                            // height: 230.h,

                            height: 230.h,
                            alignment: Alignment.topCenter,

                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: 10,
                                shrinkWrap: true,
                                physics: ScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding: const EdgeInsets.only(left: 5.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: 160.w,
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(
                                                198, 255, 255, 255),
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            boxShadow: const [
                                              BoxShadow(
                                                  color: Color.fromARGB(
                                                      255, 221, 216, 216),
                                                  blurRadius: 5,
                                                  spreadRadius: 1,
                                                  offset: Offset(4, 4)),
                                            ],
                                          ),
                                          child: Wrap(
                                            children: [
                                              Image.asset(
                                                "assets/Images/shoes.png",
                                              ),
                                              Container(
                                                // height: 70.h,
                                                // color: Colors.grey,
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 5,
                                                              left: 8,
                                                              right: 5),
                                                      child: Row(
                                                        children: [
                                                          Text(
                                                            "7,000 SAR",
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Poppins-SemiBold",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          ),
                                                          Spacer(),
                                                          Container(
                                                            height: 18.h,
                                                            width: 37.w,
                                                            decoration: BoxDecoration(
                                                                color: Color
                                                                    .fromARGB(
                                                                        255,
                                                                        221,
                                                                        30,
                                                                        30),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            8)),
                                                            child: Center(
                                                              child: Text(
                                                                "USED",
                                                                style:
                                                                    TextStyle(
                                                                  color: Color
                                                                      .fromARGB(
                                                                          255,
                                                                          248,
                                                                          246,
                                                                          246),
                                                                  fontSize: 10,
                                                                  fontFamily:
                                                                      "Poppins-Regular",
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 1,
                                                              left: 8,
                                                              right: 5),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            "Apple airpods",
                                                            textAlign:
                                                                TextAlign.left,
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 12,
                                                              fontFamily:
                                                                  "Poppins-Regular",
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 2,
                                                              left: 8,
                                                              right: 5),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            "Jeddah, Saudi Arabia",
                                                            textAlign:
                                                                TextAlign.left,
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 10,
                                                              fontFamily:
                                                                  "Poppins-Regular",
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    // Container(
                                                    //     height: 2.h,
                                                    //     color: Colors.grey),
                                                    Divider(),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 8,
                                                              right: 5,
                                                              bottom: 2),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Image.asset(
                                                              "assets/Icons/location_icon.png",
                                                              width: 16.w,
                                                              height: 17.h),
                                                          SizedBox(
                                                            height: 17.h,
                                                            width: 5.w,
                                                          ),
                                                          Text(
                                                            "The Full Cart",
                                                            textAlign:
                                                                TextAlign.left,
                                                            style: TextStyle(
                                                              color: Color
                                                                  .fromARGB(
                                                                      239,
                                                                      66,
                                                                      190,
                                                                      138),
                                                              fontSize: 10,
                                                              fontFamily:
                                                                  "Poppins-Regular",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700,
                                                            ),
                                                          ),
                                                          Spacer(),
                                                          Container(
                                                              height: 10.h,
                                                              width: 10.w,
                                                              decoration: BoxDecoration(
                                                                  color: Color
                                                                      .fromARGB(
                                                                          239,
                                                                          66,
                                                                          190,
                                                                          138),
                                                                  shape: BoxShape
                                                                      .circle)),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }),
                          ),
                        ),
                      ]),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: BottomMenu(
                    selectedIndex: 0,
                  ),
                )
              ],
            )));
  }

  final List<Widget> imageSliders = imgList
      .map((item) => Container(
            child: Container(
              margin: EdgeInsets.all(3.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Image.asset("assets/Images/card1.png")),
            ),
          ))
      .toList();
}
