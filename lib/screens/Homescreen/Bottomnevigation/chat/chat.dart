// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace, unused_import

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:tejarh/screens/Homescreen/Bottomnevigation/bottomnavigation.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/chat/chattingpage.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myprofile/myprofile.dart';

class Chat extends StatefulWidget {
  const Chat({Key? key}) : super(key: key);

  @override
  State<Chat> createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 9.0, left: 15, right: 15),
                        child: Row(
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MyProfile()),
                                );
                              },
                              child: Image.asset(
                                "assets/Images/item_avtar.png",
                                height: 36,
                                width: 36,
                              ),
                            ),
                            Spacer(),
                            const Text(
                              "Chat",
                              style: TextStyle(
                                  color: Color(0xff262E3A),
                                  fontSize: 20,
                                  fontFamily: "Poppins-SemiBold",
                                  fontWeight: FontWeight.w700),
                              textAlign: TextAlign.center,
                            ),
                            const Spacer(),
                            //
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChattingPage()),
                          );
                        },
                        child: Container(
                            height: MediaQuery.of(context).size.height,
                            child: Image.asset(
                              "assets/Images/chat_screenshot.png",
                              fit: BoxFit.contain,
                            )),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: BottomMenu(
                  selectedIndex: 1,
                ),
              )
            ],
          )),
    );
  }
}
