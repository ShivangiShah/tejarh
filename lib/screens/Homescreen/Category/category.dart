// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/bottomnavigation.dart';
import 'package:tejarh/screens/Homescreen/Category/Subcategory.dart';

class Category extends StatefulWidget {
  const Category({Key? key}) : super(key: key);

  @override
  State<Category> createState() => _CategoryState();
}

class _CategoryState extends State<Category> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
            // bottomNavigationBar: BottomMenu(),
            body: Column(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 9, left: 15),
          child: Row(
            children: [
              InkWell(
                // close keyboard on outside input tap
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  "assets/Icons/back.svg",
                  height: 21.h,
                  width: 12.w, //just like you define in pubspec.yaml file
                ),
              ),
              const Spacer(),
              const Text(
                "Categories",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontFamily: "Poppins-SemiBold",
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              ),
              const Spacer(),
              const SizedBox(
                height: 21,
                width: 12,
              ),
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.90,
          child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: 15,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SubCategory()),
                    );
                  },
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 21, left: 15, right: 15),
                    child: Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: Image.asset(
                            "assets/Images/laptop.png",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 22, right: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            // ignore: prefer_const_literals_to_create_immutables
                            children: [
                              Text(
                                "Electronics",
                                style: TextStyle(
                                    color: Color.fromARGB(255, 5, 5, 5),
                                    fontSize: 20,
                                    fontFamily: "Poppins-Medium",
                                    fontWeight: FontWeight.w500),
                              ),
                              SizedBox(width: 10),
                              Icon(Icons.arrow_forward_ios_outlined, size: 20),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }),
        )
      ],
    )));
  }
}
