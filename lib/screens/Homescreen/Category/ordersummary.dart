// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, deprecated_member_use, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:tejarh/screens/payment/payment_method.dart';

class OrderSummary extends StatefulWidget {
  const OrderSummary({Key? key}) : super(key: key);

  @override
  State<OrderSummary> createState() => _OrderSummaryState();
}

class _OrderSummaryState extends State<OrderSummary> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 15.0, right: 15),
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 9),
                    child: Row(
                      children: [
                        InkWell(
                          // close keyboard on outside input tap
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset(
                            "assets/Icons/back.svg",
                            height: 21,
                            width:
                                12, //just like you define in pubspec.yaml file
                          ),
                        ),
                        Spacer(),
                        Text(
                          "Order Summary",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        ),
                        Spacer(),
                        SizedBox(
                          height: 21,
                          width: 12,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 34.0),
                    child: Row(
                      children: [
                        Text(
                          "Shipping to",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SvgPicture.asset("assets/Icons/Location.svg",
                            width: 21.w, height: 26.h),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Text(
                            "P.O Box 401247, \n Dubai",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontFamily: "Poppins-Regular",
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Spacer(),
                        Container(
                          height: 34.h,
                          width: 86.w,
                          decoration: BoxDecoration(
                            border: Border.all(color: Color(0xff0AD188)),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(
                            child: Text(
                              "Change",
                              style: TextStyle(
                                  color: Color(0xff0AD188),
                                  fontSize: 16,
                                  fontFamily: "Poppins-Medium",
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Divider(
                      thickness: 2,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Image.asset(
                          "assets/Images/airpods.png",
                          height: 100.h,
                          // width: 146.w,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: Column(
                            children: [
                              Text(
                                "Apple airpods",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontFamily: "Poppins-Regular",
                                    fontWeight: FontWeight.w400),
                                // textAlign: TextAlign.left,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Text(
                                  "7,000 SAR",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontFamily: "Poppins-SemiBold",
                                      fontWeight: FontWeight.w600),
                                  // textAlign: TextAlign.left,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Divider(
                      thickness: 2,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Payment Details",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontFamily: "Poppins-SemiBold",
                            fontWeight: FontWeight.w600),
                        // textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Row(
                      children: [
                        Text(
                          "Item price",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w400),
                          // textAlign: TextAlign.left,
                        ),
                        Spacer(),
                        Text(
                          "7,000 SAR",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w400),
                          // textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Row(
                      children: [
                        Text(
                          "Sell tax",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w400),
                          // textAlign: TextAlign.left,
                        ),
                        Spacer(),
                        Text(
                          "60 SAR",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w400),
                          // textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Row(
                      children: [
                        Text(
                          "Shipping Charge",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w400),
                          // textAlign: TextAlign.left,
                        ),
                        Spacer(),
                        Text(
                          "10 SAR",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w400),
                          // textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Row(
                      children: [
                        Text(
                          "Discount",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w400),
                          // textAlign: TextAlign.left,
                        ),
                        Spacer(),
                        Text(
                          "-10 SAR",
                          style: TextStyle(
                              color: Color.fromARGB(255, 255, 2, 2),
                              fontSize: 16,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w400),
                          // textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Divider(
                      thickness: 2,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Row(
                      children: [
                        Text(
                          "Amount Payable",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w600),
                          // textAlign: TextAlign.left,
                        ),
                        Spacer(),
                        Text(
                          "7,060 SAR",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontFamily: "Poppins-Regular",
                              fontWeight: FontWeight.w600),
                          // textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              right: 15,
              left: 15,
              bottom: 10,
            ),
            child: Container(
              height: 57.h,
              width: double.infinity,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7)),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PaymentMethod()),
                  );
                },
                color: const Color.fromARGB(239, 66, 190, 138),
                child: const Text("Pay Now",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontFamily: "Poppins-Regular")),
              ),
            ),
          )
        ],
      ),
    ));
  }
}
