// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/bottomnavigation.dart';
import 'package:tejarh/screens/Homescreen/Category/item.dart';

class Laptop extends StatefulWidget {
  const Laptop({Key? key}) : super(key: key);

  @override
  State<Laptop> createState() => _LaptopState();
}

class _LaptopState extends State<Laptop> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
        child: Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 9, left: 15),
                    child: Row(
                      children: [
                        InkWell(
                          // close keyboard on outside input tap
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset(
                            "assets/Icons/back.svg",
                            height: 21,
                            width:
                                12, //just like you define in pubspec.yaml file
                          ),
                        ),
                        const Spacer(),
                        const Text(
                          "Laptops",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontFamily: "Poppins-SemiBold",
                              fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        ),
                        const Spacer(),
                        const SizedBox(
                          height: 21,
                          width: 12,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 20, left: 15, right: 15),
                    child: Row(
                      children: [
                        Container(
                          height: 50.h,
                          width: 285.w,
                          child: TextField(
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(15),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(28)),
                              hintText: 'Search',
                              hintStyle: TextStyle(color: Colors.grey),
                              prefixIcon: Align(
                                widthFactor: 1,
                                heightFactor: 1,
                                child: Icon(Icons.search_outlined),
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                            height: 50.h,
                            width: 50.w,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color.fromARGB(239, 66, 190, 138)),
                            child: Image.asset("assets/Icons/settings.png",
                                height: 25.h, width: 25.w)),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10.0, right: 15, left: 15),
                    child: Container(
                      // height: 470.h,
                      // color: Colors.green,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Item()),
                          );
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SingleChildScrollView(
                              child: Container(
                                // height: 470.h,
                                // height: MediaQuery.of(context).size.height * 0.58,
                                // decoration: BoxDecoration(color: Colors.white),
                                child: GridView.builder(
                                    shrinkWrap: true,
                                    physics: ScrollPhysics(),
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      childAspectRatio: 0.80.r,
                                      mainAxisSpacing: 5.r,
                                      crossAxisSpacing: 20.0.r,
                                    ),
                                    itemCount: 10,
                                    itemBuilder: (BuildContext ctx, index) {
                                      return Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: 160.w,
                                            decoration: BoxDecoration(
                                              color: Color.fromARGB(
                                                  198, 255, 255, 255),
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              boxShadow: const [
                                                BoxShadow(
                                                    color: Color.fromARGB(
                                                        255, 221, 216, 216),
                                                    blurRadius: 5,
                                                    spreadRadius: 1,
                                                    offset: Offset(4, 4)),
                                              ],
                                            ),
                                            child: Wrap(
                                              children: [
                                                Stack(
                                                  children: [
                                                    Image.asset(
                                                      "assets/Images/airpods.png",
                                                    ),
                                                    Column(
                                                      children: [
                                                        Align(
                                                          alignment: Alignment
                                                              .topRight,
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 11,
                                                                    right: 12),
                                                            child: Container(
                                                                width: 20.w,
                                                                height: 20.h,
                                                                decoration: BoxDecoration(
                                                                    shape: BoxShape
                                                                        .circle,
                                                                    color: Color
                                                                        .fromARGB(
                                                                            255,
                                                                            216,
                                                                            206,
                                                                            206)),
                                                                child: Center(
                                                                  child: SvgPicture.asset(
                                                                      "assets/Icons/heart.svg",
                                                                      height:
                                                                          11.h,
                                                                      width:
                                                                          12.w),
                                                                )),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 11),
                                                      child: Container(
                                                        height: 20.h,
                                                        width: 65.w,
                                                        decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.only(
                                                                topRight: Radius
                                                                    .circular(
                                                                        10.0),
                                                                bottomRight: Radius
                                                                    .circular(
                                                                        10.0)),
                                                            color:
                                                                Color.fromARGB(
                                                                    239,
                                                                    66,
                                                                    190,
                                                                    138)),
                                                        child: Center(
                                                          child: Text(
                                                              "FEATURED",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 10,
                                                                fontFamily:
                                                                    "Poppins-Medium",
                                                              )),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Container(
                                                  // height: 70.h,
                                                  // color: Colors.grey,
                                                  child: Column(
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                top: 5,
                                                                left: 8,
                                                                right: 5),
                                                        child: Row(
                                                          children: [
                                                            Text(
                                                              "7,000 SAR",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 14,
                                                                fontFamily:
                                                                    "Poppins-SemiBold",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ),
                                                            ),
                                                            Spacer(),
                                                            Container(
                                                              height: 18.h,
                                                              width: 37.w,
                                                              decoration: BoxDecoration(
                                                                  color: Color
                                                                      .fromARGB(
                                                                          255,
                                                                          221,
                                                                          30,
                                                                          30),
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              8)),
                                                              child: Center(
                                                                child: Text(
                                                                  "USED",
                                                                  style:
                                                                      TextStyle(
                                                                    color: Color
                                                                        .fromARGB(
                                                                            255,
                                                                            248,
                                                                            246,
                                                                            246),
                                                                    fontSize:
                                                                        10,
                                                                    fontFamily:
                                                                        "Poppins-Regular",
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                top: 1,
                                                                left: 8,
                                                                right: 5),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              "Apple airpods",
                                                              textAlign:
                                                                  TextAlign
                                                                      .left,
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 12,
                                                                fontFamily:
                                                                    "Poppins-Regular",
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                top: 2,
                                                                left: 8,
                                                                right: 5),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              "Jeddah, Saudi Arabia",
                                                              textAlign:
                                                                  TextAlign
                                                                      .left,
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 10,
                                                                fontFamily:
                                                                    "Poppins-Regular",
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      // Container(
                                                      //     height: 2.h,
                                                      //     color: Colors.grey),
                                                      Divider(),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 8,
                                                                right: 5,
                                                                bottom: 2),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Image.asset(
                                                                "assets/Icons/location_icon.png",
                                                                width: 16.w,
                                                                height: 17.h),
                                                            SizedBox(
                                                              height: 17.h,
                                                              width: 5.w,
                                                            ),
                                                            Text(
                                                              "The Full Cart",
                                                              textAlign:
                                                                  TextAlign
                                                                      .left,
                                                              style: TextStyle(
                                                                color: Color
                                                                    .fromARGB(
                                                                        239,
                                                                        66,
                                                                        190,
                                                                        138),
                                                                fontSize: 10,
                                                                fontFamily:
                                                                    "Poppins-Regular",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700,
                                                              ),
                                                            ),
                                                            Spacer(),
                                                            Container(
                                                                height: 10.h,
                                                                width: 10.w,
                                                                decoration: BoxDecoration(
                                                                    color: Color
                                                                        .fromARGB(
                                                                            239,
                                                                            66,
                                                                            190,
                                                                            138),
                                                                    shape: BoxShape
                                                                        .circle)),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      );
                                    }),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: BottomMenu(
              selectedIndex: 0,
            ),
          )
        ],
      ),
    ));
  }
}
