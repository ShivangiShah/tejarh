// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/Homescreen/Category/laptop.dart';

class SubCategory extends StatefulWidget {
  const SubCategory({Key? key}) : super(key: key);

  @override
  State<SubCategory> createState() => _SubCategoryState();
}

class _SubCategoryState extends State<SubCategory> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 9, left: 15),
              child: Row(
                children: [
                  InkWell(
                    // close keyboard on outside input tap
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: SvgPicture.asset(
                      "assets/Icons/back.svg",
                      height: 21,
                      width: 12, //just like you define in pubspec.yaml file
                    ),
                  ),
                  const Spacer(),
                  const Text(
                    "Subcategories",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontFamily: "Poppins-SemiBold",
                        fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                  const Spacer(),
                  const SizedBox(
                    height: 21,
                    width: 12,
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.90,
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: 15,
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                           MaterialPageRoute(builder: (context) => Laptop()),
                        );
                      },
                      child: Padding(
                        padding:
                            const EdgeInsets.only(top: 21, right: 15, left: 15),
                        child: Stack(
                          children: [
                            Container(
                              height: 72.h,
                              width: 345.w,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: const [
                                  BoxShadow(
                                      color: Color.fromARGB(176, 158, 158, 158),
                                      blurRadius: 10,
                                      spreadRadius: 0,
                                      offset: Offset(0, 1)),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 11, left: 8),
                                  child: Image.asset(
                                      "assets/Icons/avtar_laptop.png",
                                      height: 50.h,
                                      width: 50.w),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 20, top: 10),
                                  child: Text(
                                    "Laptops",
                                    style: TextStyle(
                                        color: Color.fromARGB(255, 5, 5, 5),
                                        fontSize: 20,
                                        fontFamily: "Poppins-Medium",
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
