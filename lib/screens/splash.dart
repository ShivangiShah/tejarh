// ignore_for_file: prefer_const_constructors

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:tejarh/screens/Authentication/Login/view/homescreen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    // ignore: unnecessary_new
    new Future.delayed(
        const Duration(seconds: 3),
        () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const HomeScreen()),
            ));
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.dark, // status bar icons' color
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Color.fromARGB(0, 141, 132, 132),
        statusBarColor:
            Color.fromARGB(0, 141, 132, 132) //navigation bar icons' color
        ));
    return Scaffold(
        body: Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Stack(children: <Widget>[
        SvgPicture.asset(
          "assets/Images/background.svg",
          fit: BoxFit.fill,
        ),
        Align(
          alignment: Alignment.center,
          child: Image.asset(
            "assets/Images/Tlogo.png",
            height: 301,
            width: 173,
          ),
        ),
      ]),
    ));
  }
}
