
import 'dart:convert';

SignupModel signupModelFromJson(String str) => SignupModel.fromJson(json.decode(str));

String signupModelToJson(SignupModel data) => json.encode(data.toJson());

class SignupModel {
    SignupModel({
        this.success,
        this.statusCode,
        this.message,
        this.data,
    });

    bool? success;
    int? statusCode;
    String? message;
    Data? data;

    factory SignupModel.fromJson(Map<String, dynamic> json) => SignupModel(
        success: json["success"],
        statusCode: json["status_code"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "status_code": statusCode,
        "message": message,
        "data": data!.toJson(),
    };
}

class Data {
    Data({
        this.profilePicture,
        this.firstName,
        this.lastName,
        this.username,
        this.email,
        this.phoneNumber,
        this.password,
        this.role,
        this.status,
        this.isConfirm,
        this.otp,
        this.updatedAt,
        this.createdAt,
        this.id,
    });

    dynamic? profilePicture;
    String? firstName;
    String? lastName;
    String? username;
    String? email;
    String? phoneNumber;
    String? password;
    int? role;
    bool? status;
    bool? isConfirm;
    int? otp;
    DateTime? updatedAt;
    DateTime? createdAt;
    int? id;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        profilePicture: json["profile_picture"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        username: json["username"],
        email: json["email"],
        phoneNumber: json["phone_number"],
        password: json["password"],
        role: json["role"],
        status: json["status"],
        isConfirm: json["is_confirm"],
        otp: json["otp"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "profile_picture": profilePicture,
        "first_name": firstName,
        "last_name": lastName,
        "username": username,
        "email": email,
        "phone_number": phoneNumber,
        "password": password,
        "role": role,
        "status": status,
        "is_confirm": isConfirm,
        "otp": otp,
        "updated_at": updatedAt!.toIso8601String(),
        "created_at": createdAt!.toIso8601String(),
        "id": id,
    };
}
