// import 'package:flutter/material.dart';

// ignore_for_file: non_constant_identifier_names, unused_import, avoid_print

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:tejarh/models/BaseModel.dart';
import 'package:tejarh/network/api_endpoint.dart';
import 'package:tejarh/screens/Authentication/Login/view/homescreen.dart';
import 'package:tejarh/screens/Authentication/Signup/models/SignupModel.dart';
import 'package:tejarh/utils/preference_utils.dart';
import 'package:tejarh/utils/share_predata.dart';
import 'package:tejarh/widgets/commonwidget.dart';

TextEditingController inputText = TextEditingController();
TextEditingController pswdText = TextEditingController();
TextEditingController lastnameController = TextEditingController();
TextEditingController firstnameController = TextEditingController();
TextEditingController usernameController = TextEditingController();
TextEditingController emailController = TextEditingController();
TextEditingController numberController = TextEditingController();
TextEditingController passwordController = TextEditingController();
String posterpath = '';

Future<void> RegisterApi(BuildContext context) async {
  var headers = {'Content-Type': 'application/json'};
  var request = http.MultipartRequest('POST', Uri.parse(urlBase + urlSignup));

  if (posterpath != "") {
    request.files.add(await http.MultipartFile.fromPath(
        'profile_picture', posterpath.toString()));

    request.fields.addAll({
      'first_name': firstnameController.value.text,
      'last_name': lastnameController.value.text,
      'username': usernameController.value.text,
      'email': emailController.value.text,
      'phone_number': numberController.value.text,
      'password': passwordController.value.text,
    });

    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      await response.stream.bytesToString().then((value) async {
        String strData = value;
        Map<String, dynamic> userModel = json.decode(strData);
        BaseModel model = BaseModel.fromJson(userModel);

        if (model.statusCode == 200) {
          SignupModel? signUp = SignupModel.fromJson(userModel);

          var preferences = MySharedPref();
          await preferences.setSignupModel(signUp, SharePreData.keySignupModel);

          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const HomeScreen()),
          );
        } else {
          print(model.message!);
        }
      });
    } else {
      print(response.reasonPhrase);
    }
  }
}