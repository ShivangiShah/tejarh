import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:tejarh/screens/Authentication/Signup/view/business_account.dart';
import 'package:tejarh/screens/Authentication/Signup/view/user_account.dart';

class UserSignup extends StatefulWidget {
  const UserSignup({Key? key}) : super(key: key);

  @override
  State<UserSignup> createState() => _UserSignupState();
}

class _UserSignupState extends State<UserSignup>
    with SingleTickerProviderStateMixin {
  TabController? tabController;
  int activeIndex =0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(length: 2, vsync: this);
    tabController!.addListener(() {
      activeIndex = tabController!.index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: [
      Stack(children: <Widget>[
        SvgPicture.asset(
          "assets/Images/cutbg.svg",
          // height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context)
              .size
              .width, //just like you define in pubspec.yaml file
        ),
        Positioned(
          top: MediaQuery.of(context).size.width * 0.20,
          left: MediaQuery.of(context).size.width * 0.42,
          child: Center(
            child: Image.asset(
              "assets/Images/Tlogo.png",
              height: 96,
              width: 55,
            ), //just like you define in pubspec.yaml file
          ),
        ),
      ]),
      const Text(
        "CREATE ACCOUNT,",
        style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontFamily: "Poppins-Bold",
            fontWeight: FontWeight.w700),
        textAlign: TextAlign.center,
      ),
      const Padding(
        padding: EdgeInsets.only(top: 5),
        child: Text(
          " Please Sign up to get started!",
          style: TextStyle(
            color: Colors.black,
            fontSize: 16,
            fontFamily: "Poppins-Regular",
            //  fontWeight: FontWeight.w500
          ),
          textAlign: TextAlign.center,
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(top: 15),
        child: Container(
          child: TabBar(
            tabs: [
              Tab(
                child: Container(
                  height: 66,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      border: Border.all(
                        color: Colors.grey,
                        width: 1,
                      )),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: const Icon(
                          Icons.person,
                        ),
                      ),
                      SizedBox(width: 5),
                      const Text(
                        'User',
                        maxLines: 1,
                        style: TextStyle(
                          overflow: TextOverflow.ellipsis,
                          fontFamily: "Poppins-Regular",
                          fontSize: 16,
                          // letterSpacing: -0.28,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Tab(
                child: Container(
                  height: 50,
                  width: 165,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7),
                      border: Border.all(
                        color: Colors.grey,
                        width: 1,
                      )),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: const Icon(
                          Icons.business_center_outlined,
                        ),
                      ),
                      SizedBox(width: 5),
                      const Text(
                        'Business',
                        maxLines: 1,
                        style: TextStyle(
                          overflow: TextOverflow.ellipsis,
                          fontFamily: "Poppins-Regular",
                          fontSize: 16,
                          // letterSpacing: -0.28,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
            indicator: BoxDecoration(
              color: const Color.fromARGB(239, 66, 190, 138),
              borderRadius: BorderRadius.circular(7),
            ),
            indicatorSize: TabBarIndicatorSize.label,
            unselectedLabelColor: Colors.grey,
            labelColor: Colors.white,
            controller: tabController,
          ),
        ),
      ),
      Expanded(
        child: TabBarView(
          controller: tabController,
          children: const [
            UserTab(),
            BusinessTab(),
          ],
        ),
      ),
    ]));
  }
}
