// ignore_for_file: deprecated_member_use, sized_box_for_whitespace, prefer_const_constructors, unused_import, prefer_const_literals_to_create_immutables, avoid_print

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tejarh/Utility/Global.dart';
import 'package:tejarh/screens/Authentication/Signup/controller/SignupController.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/homepage/homepage.dart';
import 'package:tejarh/widgets/commonwidget.dart';
import 'package:image_picker/image_picker.dart';

class UserTab extends StatefulWidget {
  const UserTab({Key? key}) : super(key: key);

  @override
  State<UserTab> createState() => _UserTabState();
}

class _UserTabState extends State<UserTab> {
  var isPassword = false;
  var isConfirmEmpty = false;
  var isPasswordInvalid = false;
  var isPasswordRegex = false;
  var isEmailValidation = false;
  var isPhoneValidation = false;
  var isPhoneEmpty = false;
  var isPhoneInvalid = false;
  bool isHidden = true;
  bool isHidden2 = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Padding(
      padding: EdgeInsets.only(left: 15, right: 15),
      child: Column(children: [
        Padding(
          padding: EdgeInsets.only(top: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              poster.toString() != "File: ''"
                  ? InkWell(
                      onTap: () {
                        setState(() {
                          selectPhoto();
                        });
                      },
                      child: Container(
                        height: 108.h,
                        width: 108.w,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(75.0),
                          child: Image.file(
                            poster,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                    )
                  : InkWell(
                      onTap: () {
                        selectPhoto();
                      },
                      child: Image.asset(
                        "assets/Icons/Group.png",
                        height: 108,
                        width: 108,
                      ),
                    ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 20),
          child: TextField(
            controller: firstnameController,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'First Name',
                hintStyle: TextStyle(color: Colors.grey)),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 15),
          child: TextField(
            controller: lastnameController,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Last Name',
                hintStyle: TextStyle(color: Colors.grey)),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 15),
          child: TextField(
            controller: usernameController,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'User Name',
                hintStyle: TextStyle(color: Colors.grey)),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 15),
          child: TextField(
            controller: emailController,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Email',
                hintStyle: TextStyle(color: Colors.grey)),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 77,
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: '+91',
                    hintStyle: TextStyle(color: Colors.grey),
                    suffixIcon: Align(
                      widthFactor: 1.0,
                      heightFactor: 1.0,
                      child: Icon(
                        Icons.expand_more_sharp,
                      ),
                    ),
                  ),
                ),
              ),
              Container(width: 19),
              Expanded(
                child: TextField(
                  controller: numberController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Enter Phone Number',
                      hintStyle: TextStyle(color: Colors.grey)),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 15),
          child: TextField(
            controller: passwordController,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Password',
              hintStyle: TextStyle(color: Colors.grey),
              suffixIcon: Align(
                widthFactor: 1.0,
                heightFactor: 1.0,
                child: Icon(
                  Icons.visibility_off_outlined,
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 15),
          child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Confirm Password',
                hintStyle: TextStyle(color: Colors.grey)),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 15),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(color: Colors.black, fontSize: 14),
              children: <TextSpan>[
                TextSpan(
                  text: 'By creating an account. You agree to the \n',
                ),
                TextSpan(
                    text: 'Term of Service ',
                    style: TextStyle(color: Colors.blue)),
                TextSpan(
                  text: 'and acknowledge our \n',
                ),
                TextSpan(
                    text: ' Privacy Policy.',
                    style: TextStyle(color: Colors.blue))
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 15),
          child: SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              height: 57,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7)),
                onPressed: () {
                  RegisterApi(context);
                },
                color: const Color.fromARGB(239, 66, 190, 138),
                child: const Text("Sign Up",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontFamily: "Poppins-Regular")),
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20, bottom: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Already have a account?  ",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: "Poppins-Regular",
                ),
              ),
              InkWell(
                onTap: () {},
                child: Text(
                  "Sign In",
                  style: TextStyle(
                      color: Color.fromARGB(239, 66, 190, 138),
                      fontSize: 16,
                      fontFamily: "Poppins-Medium",
                      fontWeight: FontWeight.w700),
                ),
              )
            ],
          ),
        ),
      ]),
    )));
  }

  selectPhoto() {
    showImagePicker(context).then((value) {
      setState(() {
        if (imagePath.toString() != "File: ''") {
          poster = imagePath;
          posterpath = imagePath.path.toString();
        }
      });
      print(imagePath.toString());
    });
  }
}
