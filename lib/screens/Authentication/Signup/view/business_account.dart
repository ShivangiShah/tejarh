// ignore_for_file: deprecated_member_use, avoid_unnecessary_containers, prefer_const_constructors, sized_box_for_whitespace, unused_import

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tejarh/screens/Authentication/Login/view/homescreen.dart';
import 'package:tejarh/screens/Authentication/Signup/view/businessStore_tab.dart';

class BusinessTab extends StatefulWidget {
  const BusinessTab({Key? key}) : super(key: key);

  @override
  State<BusinessTab> createState() => _BusinessTabState();
}

class _BusinessTabState extends State<BusinessTab> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/Icons/Group.png",
                  height: 108,
                  width: 108,
                ),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 20),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Company Name',
                  hintText: 'Company Name',
                  hintStyle: TextStyle(color: Colors.grey)),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 15),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Company Legal Name',
                  hintText: 'Company Legal Name',
                  hintStyle: TextStyle(color: Colors.grey)),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 15),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Owner / Manager Name',
                  hintText: 'Owner / Manager Name',
                  hintStyle: TextStyle(color: Colors.grey)),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 15),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Enter CR Number',
                  hintText: 'Enter CR Number',
                  hintStyle: TextStyle(color: Colors.grey)),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 15),
              child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Upload CR',
                      hintText: 'Upload CR',
                      hintStyle: TextStyle(color: Colors.grey),
                      suffixIcon: Align(
                          widthFactor: 1.0,
                          heightFactor: 1.0,
                          child: Icon(CupertinoIcons.paperclip))))),
          Padding(
            padding: EdgeInsets.only(top: 15),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Enter Maroof Number',
                  hintText: 'Enter Maroof Number',
                  hintStyle: TextStyle(color: Colors.grey)),
            ),
          ),
           Padding(
            padding: EdgeInsets.only(top: 15),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Upload Maroof',
                  hintText: 'Upload Maroof',
                  hintStyle: TextStyle(color: Colors.grey),
                  suffixIcon: Align(
                      widthFactor: 1.0,
                      heightFactor: 1.0,
                      child: Icon(CupertinoIcons.paperclip))),
            ),
          ),
         Padding(
            padding: EdgeInsets.only(top: 15),
            child: TextField(
                decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Date of expiry',
              hintText: 'Date of expiry',
              hintStyle: TextStyle(color: Colors.grey),
              suffixIcon: Padding(
                padding: EdgeInsetsDirectional.only(end: 10),
                child: Icon(
                  Icons.calendar_today,
                ),
              ),
            )),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 15),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'VAT Number',
                  hintText: 'VAT Number',
                  hintStyle: TextStyle(color: Colors.grey),
                  suffixIcon: Align(
                      widthFactor: 1.0,
                      heightFactor: 1.0,
                      child: Icon(CupertinoIcons.paperclip))),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 15),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Bank Name',
                  hintText: 'Bank Name',
                  hintStyle: TextStyle(color: Colors.grey)),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 15),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Bank Account Number',
                  hintText: 'Bank Account Number',
                  hintStyle: TextStyle(color: Colors.grey)),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 15),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'IBAN number',
                  hintText: 'IBAN number',
                  hintStyle: TextStyle(color: Colors.grey)),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 15),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Business email',
                  hintText: 'Business email',
                  hintStyle: TextStyle(color: Colors.grey)),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 77,
                  child: const TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: '+91',
                      hintStyle: TextStyle(color: Colors.grey),
                      suffixIcon: Align(
                        widthFactor: 1.0,
                        heightFactor: 1.0,
                        child: Icon(
                          Icons.expand_more_sharp,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(width: 19),
                const Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Enter Phone Number',
                        hintText: 'Enter Phone Number',
                        hintStyle: TextStyle(color: Colors.grey)),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.9,
                height: 57,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7)),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const BusinessStoreTab()),
                    );
                  },
                  color: const Color.fromARGB(239, 66, 190, 138),
                  child: const Text("Next",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontFamily: "Poppins-Regular")),
                )),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20, bottom: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Already have a account?  ",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => HomeScreen()),
                    );
                  },
                  child: Text(
                    "Sign In",
                    style: TextStyle(
                        color: Color.fromARGB(239, 66, 190, 138),
                        fontSize: 16,
                        fontFamily: "Poppins-Medium",
                        fontWeight: FontWeight.w700),
                  ),
                ),
              ],
            ),
          ),
        ]),
      ), 
    ));
  }
}