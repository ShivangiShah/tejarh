// ignore_for_file: deprecated_member_use, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:tejarh/screens/Authentication/Login/view/forgot_pass.dart';
import 'package:tejarh/screens/Authentication/Login/view/verification_otp.dart';
import 'package:tejarh/screens/Authentication/Signup/view/user_signup.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/homepage/homepage.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  get child => null;
  get height => null;
  get onPressed => null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
      children: [
        Stack(children: <Widget>[
          SvgPicture.asset(
            "assets/Images/cutbg.svg",
            // height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context)
                .size
                .width, //just like you define in pubspec.yaml file
          ),
          Positioned(
            top: MediaQuery.of(context).size.width * 0.20,
            left: MediaQuery.of(context).size.width * 0.42,
            child: Center(
              child: Image.asset(
                "assets/Images/Tlogo.png",
                height: 96,
                width: 55,
              ), //just like you define in pubspec.yaml file
            ),
          ),
        ]),
        Text(
          "WELCOME BACK!",
          style: TextStyle(
              color: Colors.black,
              fontSize: 20,
              fontFamily: "Poppins-Bold",
              fontWeight: FontWeight.w700),
          textAlign: TextAlign.center,
        ),
        Padding(
          padding: EdgeInsets.only(top: 5),
          child: Text(
            " Please sign in to your account.",
            style: TextStyle(
              color: Colors.black,
              fontSize: 16,
              fontFamily: "Poppins-Regular",
              //  fontWeight: FontWeight.w500
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 20, right: 15, left: 15),
          child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'User Name/Email',
                hintText: 'User Name/Email',
                hintStyle: TextStyle(color: Colors.grey)),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 15, right: 15, left: 15),
          child: TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Password',
              hintText: 'Password',
              hintStyle: TextStyle(color: Colors.grey),
              suffixIcon: Align(
                widthFactor: 1.0,
                heightFactor: 1.0,
                child: Icon(
                  Icons.remove_red_eye,
                ),
              ),
            ),
          ),
        ),
        Padding(
            padding: EdgeInsets.only(top: 26, right: 15, left: 15),
            child: Row(children: [
              Icon(Icons.check_box),
              SizedBox(width: 5),
              Text(
                "Remember Me",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: "Poppins-Regular",
                ),
              ),
              Spacer(),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const Forgot_pass()),
                  );
                },
                child: Text(
                  "Forgot Password?",
                  style: TextStyle(
                      color: Color.fromARGB(239, 66, 190, 138),
                      fontSize: 16,
                      fontFamily: "Poppins-Medium",
                      fontWeight: FontWeight.w700),
                ),
              )
            ])),
        Padding(
          padding: EdgeInsets.only(top: 21, right: 15, left: 15),
        ),
        SizedBox(
            width: MediaQuery.of(context).size.width * 0.9,
            height: 57,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7)),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => VerificationOtp()),
                );
              },
              color: Color.fromARGB(239, 66, 190, 138),
              child: Text("Sign in",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: "Poppins-Regular")),
            )),
        Padding(
          padding: EdgeInsets.only(top: 23),
        ),
        InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePage()),
            );
          },
          child: Text(
            "Skip to homepage",
            style: TextStyle(
                color: Color.fromARGB(239, 66, 190, 138),
                fontSize: 16,
                fontFamily: "Poppins-Regular",
                fontWeight: FontWeight.w500),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 33),
        ),
        Text(
          "Or Sign in with",
          style: TextStyle(
              fontSize: 18,
              fontFamily: "Poppins-Regular",
              fontWeight: FontWeight.w500),
        ),
        Padding(padding: EdgeInsets.only(top: 15)),
        Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          SizedBox(child: SvgPicture.asset("assets/Icons/facebook.svg")),
          SizedBox(width: 26),
          SizedBox(child: SvgPicture.asset("assets/Icons/Google.svg")),
        ]),
        Padding(
          padding: EdgeInsets.only(top: 54, bottom: 23),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Don't have an account?  ",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: "Poppins-Regular",
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const UserSignup()),
                  );
                },
                child: Text(
                  "Sign Up",
                  style: TextStyle(
                    color: Color.fromARGB(239, 66, 190, 138),
                    fontSize: 18,
                    fontFamily: "Poppins-Medium",
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    )));
  }
}
