// ignore_for_file: deprecated_member_use, prefer_const_constructors, unused_import

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/Authentication/Login/view/homescreen.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/homepage/homepage.dart';

// ignore: camel_case_types
class Reset_pass extends StatefulWidget {
  const Reset_pass({Key? key}) : super(key: key);

  @override
  State<Reset_pass> createState() => _Reset_passState();
}

// ignore: camel_case_types
class _Reset_passState extends State<Reset_pass> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
      //  mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 50, left: 15),
          child: Row(
            children: [
              InkWell(
                // close keyboard on outside input tap
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  "assets/Icons/back.svg",
                  height: 21,
                  width: 12, //just like you define in pubspec.yaml file
                ),
              ),
              Spacer(),
              Padding(padding: EdgeInsets.only(top: 50)),
              Text(
                "Reset Password",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontFamily: "Poppins-SemiBold",
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              ),
              Spacer(),
              SizedBox(
                height: 21,
                width: 12,
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 40),
          child: Center(
            child: Text(
              "Your new password must be different \n from previous used passwords.",
              style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Poppins-Regular",
                //  fontWeight: FontWeight.w500
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 30, right: 15, left: 15),
          child: TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Enter New Password',
              hintText: 'Enter New Password',
              hintStyle: TextStyle(color: Colors.grey),
              suffixIcon: Align(
                widthFactor: 1.0,
                heightFactor: 1.0,
                child: Icon(
                  Icons.visibility_off_outlined,
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 15, right: 15, left: 15),
          child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Confirm New Password',
                hintText: 'Confirm New Password',
                hintStyle: TextStyle(color: Colors.grey)),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 20, right: 15, left: 15),
          child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 57,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7)),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomeScreen()),
                  );
                },
                color: Color.fromARGB(239, 66, 190, 138),
                child: Text("Reset Password",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontFamily: "Poppins-Regular")),
              )),
        ),
      ],
    )));
  }
}