// ignore_for_file: deprecated_member_use, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/homepage/homepage.dart';

// ignore: camel_case_types
class VerificationOtp extends StatefulWidget {
  const VerificationOtp({Key? key}) : super(key: key);

  @override
  State<VerificationOtp> createState() => VerificationOtpState();
}

// ignore: camel_case_types
class VerificationOtpState extends State<VerificationOtp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          child: Column(
              //  mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 50, left: 15),
              child: Row(
                children: [
                  InkWell(
                    // close keyboard on outside input tap
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: SvgPicture.asset(
                      "assets/Icons/back.svg",
                      height: 21,
                      width: 12, //just like you define in pubspec.yaml file
                    ),
                  ),
                  Spacer(),
                   Padding(padding: EdgeInsets.only(top: 50)),
                   Text(
                    "Verification",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontFamily: "Poppins-SemiBold",
                        fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                  Spacer(),
                  SizedBox(
                    height: 21,
                    width: 12,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 42),
              child: Center(
                child: Text(
                  "You will get OPT via SMS",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontFamily: "Poppins-Regular",
                    //  fontWeight: FontWeight.w500
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            // ignore: prefer_const_constructors
            Padding(
              padding: const EdgeInsets.only(top: 27, left: 21, right: 21),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    height: 57,
                    width: 57,
                    child: TextField(
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(1),
                      ],
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).nextFocus();
                        }
                      },
                      decoration:  InputDecoration(
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 57,
                    width: 57,
                    child: TextField(
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(1),
                      ],
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).nextFocus();
                        }
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 57,
                    width: 57,
                    child: TextField(
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(1),
                      ],
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).nextFocus();
                        }
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 57,
                    width: 57,
                    child: TextField(
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(1),
                      ],
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).nextFocus();
                        }
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 57,
                    width: 57,
                    child: TextField(
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(1),
                      ],
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).nextFocus();
                        }
                      },
                      decoration:  InputDecoration(
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding:  EdgeInsets.only(top: 20, right: 15, left: 15),
              child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 57,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7)),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => HomePage()),
                      );
                    },
                    color:  Color.fromARGB(239, 66, 190, 138),
                    child: Text("Verify",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontFamily: "Poppins-Regular")),
                  )),
            ),
           Padding(
              padding: EdgeInsets.only(top: 30),
              child: Center(
                child: Text(
                  "Didn't receive the verification OTP?",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                    //  fontWeight: FontWeight.w500
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
             Padding(
              padding: EdgeInsets.only(top: 5),
              child: Center(
                child: Text(
                  "Resend OTP",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: Color.fromARGB(239, 66, 190, 138),
                    fontSize: 16,
                    fontFamily: "Poppins-Regular",
                    //  fontWeight: FontWeight.w500
                  ),
                ),
              ),
            ),
          ])),
    );
  }
}
