// ignore_for_file: deprecated_member_use, unnecessary_const, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:tejarh/screens/Authentication/Login/view/reset_pass.dart';

// ignore: camel_case_types
class Forgot_pass extends StatefulWidget {
  const Forgot_pass({Key? key}) : super(key: key);

  @override
  State<Forgot_pass> createState() => _Forgot_passState();
}

// ignore: camel_case_types
class _Forgot_passState extends State<Forgot_pass> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
      //  mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 50, left: 15),
          child: Row(
            children: [
              InkWell(
                // close keyboard on outside input tap
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  "assets/Icons/back.svg",
                  height: 21,
                  width: 12, //just like you define in pubspec.yaml file
                ),
              ),
              const Spacer(),
              const Padding(padding: EdgeInsets.only(top: 50)),
              const Text(
                "Forgot Password",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontFamily: "Poppins-SemiBold",
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              ),
              const Spacer(),
              const SizedBox(
                height: 21,
                width: 12,
              ),
            ],
          ),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 40),
          child: Center(
            child: Text(
              "Enter your registered email below \n to receive password reset instruction",
              style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontFamily: "Poppins-Regular",
                //  fontWeight: FontWeight.w500
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 30, right: 15, left: 15),
          child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'User Name/Email',
                hintStyle: TextStyle(color: Colors.grey)),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 20, right: 15, left: 15),
          child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 57,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7)),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Reset_pass()),
                  );
                },
                color: const Color.fromARGB(239, 66, 190, 138),
                child: const Text("Reset Password",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontFamily: "Poppins-Regular")),
              )),
        ),
      ],
    )));
  }
}