// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:tejarh/screens/Authentication/Login/view/homescreen.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/bottomnavigation.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/hold%20an%20offer/holdanoffer.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myitems/myitems.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myorders/myorders.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/myprofile/myprofile.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/wallet/wallet.dart';
import 'package:tejarh/screens/Homescreen/Bottomnevigation/drawer/wishlist/wishlist.dart';

class MyDrawer extends StatefulWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  State<MyDrawer> createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Color.fromARGB(255, 255, 253, 253),
      statusBarColor: Color.fromARGB(255, 252, 249, 249), // status bar color
      statusBarIconBrightness: Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          Brightness.dark, //navigation bar icons' color
    ));

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        // height: MediaQuery.of(context).size.height,
                        padding: EdgeInsets.all(20),
                        color: Colors.white,
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 34.0),
                              child: Container(
                                width: 112.w,
                                height: 112.h,
                                // color: Colors.black,
                                child: Stack(
                                  children: [
                                    Center(
                                      child: Container(
                                        height: 112.h,
                                        decoration: BoxDecoration(
                                            // color: Colors.amber,
                                            // shape: BoxShape.circle,
                                            ),
                                        child: Image.asset(
                                            "assets/Images/item_avtar.png",
                                            height: 112.h),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          right: 8.0, bottom: 8),
                                      child: Align(
                                        alignment: Alignment.bottomRight,
                                        child: Container(
                                          width: 20.w,
                                          height: 20.h,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Color(0xff0AD188)),
                                              shape: BoxShape.circle,
                                              color: Color(0xff0AD188)),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 15.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "John Doe",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontFamily: "Poppins-SemiBold",
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 9.0),
                                    child: Image.asset(
                                      "assets/Images/award.png",
                                      height: 27.h,
                                      width: 27.w,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Member since jan 2020",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontFamily: "Poppins-Regular",
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 7.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset("assets/Icons/Location.svg"),
                                  SizedBox(width: 10),
                                  Text(
                                    "Riyadh, Saudi Arabia",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontFamily: "Poppins-Regular",
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 30.0, left: 22),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SvgPicture.asset("assets/Icons/D_home.svg",
                                      height: 27.h,
                                      width: 27.w,
                                      color: Color(0xff0AD188)),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15.0, top: 8),
                                    child: Text(
                                      "Home",
                                      style: TextStyle(
                                          color: Color(0xff262E3A),
                                          fontSize: 16,
                                          fontFamily: "Poppins-Medium",
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 20.0, left: 22),
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MyProfile()),
                                  );
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SvgPicture.asset(
                                      "assets/Icons/D_myprofile.svg",
                                      height: 27.h,
                                      width: 27.w,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 15.0, top: 8),
                                      child: Text(
                                        "Profile",
                                        style: TextStyle(
                                            color: Color(0xff262E3A),
                                            fontSize: 16,
                                            fontFamily: "Poppins-Medium",
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 20.0, left: 22),
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MyOrders()),
                                  );
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SvgPicture.asset(
                                      "assets/Icons/D_myorders.svg",
                                      height: 25.h,
                                      width: 25.w,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 13.0, top: 8),
                                      child: Text(
                                        "My Orders",
                                        style: TextStyle(
                                            color: Color(0xff262E3A),
                                            fontSize: 16,
                                            fontFamily: "Poppins-Medium",
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 20.0, left: 22),
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MyItems()),
                                  );
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SvgPicture.asset(
                                        "assets/Icons/D_myitems.svg",
                                        height: 30.h,
                                        width: 30.w),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 10.0, top: 8),
                                      child: Text(
                                        "My Items",
                                        style: TextStyle(
                                            color: Color(0xff262E3A),
                                            fontSize: 16,
                                            fontFamily: "Poppins-Medium",
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 20.0, left: 22),
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Wallet()),
                                  );
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SvgPicture.asset(
                                        "assets/Icons/D_wallet.svg",
                                        height: 27.h,
                                        width: 27.w),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 15.0, top: 8),
                                      child: Text(
                                        "Wallet",
                                        style: TextStyle(
                                            color: Color(0xff262E3A),
                                            fontSize: 16,
                                            fontFamily: "Poppins-Medium",
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 20.0, left: 22),
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => HoldAnOffer()),
                                  );
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SvgPicture.asset(
                                        "assets/Icons/D_holdanoffer.svg",
                                        height: 27.h,
                                        width: 27.w),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 15.0, top: 8),
                                      child: Text(
                                        "Hold an Offer",
                                        style: TextStyle(
                                            color: Color(0xff262E3A),
                                            fontSize: 16,
                                            fontFamily: "Poppins-Medium",
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 20.0, left: 22),
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => WishList()),
                                  );
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SvgPicture.asset(
                                        "assets/Icons/D_wishlist.svg",
                                        height: 25.h,
                                        width: 25.w),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 15.0, top: 8),
                                      child: Text(
                                        "Wishlist",
                                        style: TextStyle(
                                            color: Color(0xff262E3A),
                                            fontSize: 16,
                                            fontFamily: "Poppins-Medium",
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 20.0, left: 22),
                              child: InkWell(
                                onTap: () {
                                  // Navigator.push(
                                  //   context,
                                  //   MaterialPageRoute(
                                  //       builder: (context) => HoldAnOffer()),
                                  // );
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SvgPicture.asset(
                                        "assets/Icons/D_settings.svg",
                                        height: 27.h,
                                        width: 27.w),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 15.0, top: 8),
                                      child: Text(
                                        "Settings",
                                        style: TextStyle(
                                            color: Color(0xff262E3A),
                                            fontSize: 16,
                                            fontFamily: "Poppins-Medium",
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 20.0, left: 22),
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => HomeScreen()),
                                  );
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Image.asset("assets/Icons/logout.png",
                                        height: 25.h, width: 25.w),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 15.0, top: 8),
                                      child: Text(
                                        "Log Out",
                                        style: TextStyle(
                                            color: Color(0xff262E3A),
                                            fontSize: 16,
                                            fontFamily: "Poppins-Medium",
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            // Padding(
                            //   padding: const EdgeInsets.only(top: 45.0),
                            //   child: InkWell(
                            //     onTap: () {
                            //       Navigator.push(
                            //         context,
                            //         MaterialPageRoute(
                            //             builder: (context) => HomeScreen()),
                            //       );
                            //     },
                            // child: Container(
                            //   height: 58.h,
                            //   decoration: BoxDecoration(
                            //     color: Color(0xff0AD188),
                            //     shape: BoxShape.circle,
                            //   ),
                            //   child: Padding(
                            //     padding: const EdgeInsets.all(14.0),
                            //     child: Image.asset(
                            //         "assets/Icons/D_exit.png"),
                            //   ),
                            // ),
                            //   ),
                            // )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: BottomMenu(
                selectedIndex: 4,
              ),
            )
          ],
        ),
      ),
    );
  }
}
