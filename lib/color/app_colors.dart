import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xff26617A);
  static const Color transparentColor = Colors.transparent;
  static const Color textFieldBackColor = Color(0xffF4F6F6);
  static const Color textFieldColor = Color(0xffDBDDDD);
  static const Color selectedColor = Color(0xff6B6B6B);
  static const Color deselectedColor = Color(0xff9E9E9E);
  static const Color cardColor = Color(0xff555555);
  static const Color meditateColor = Color(0xff9C9C9C);
  static const Color shadowColor = Color(0xffDBDDDD);
  static const Color circleColor = Color(0xff5C5C5C);
  static const Color fontColor = Color(0xFF2041FD);
  static const Color buttonColor = Color(0xFF2DE4A7);

  // Frequently Used Colors
  static const Color whiteColor = Colors.white;
  static const Color blackColor = Colors.black;
  static const Color greyColor = Color.fromARGB(255, 187, 179, 179);
  static const Color orangeColor = Colors.orange;
  static const Color blueColor = Colors.blue;
  static const Color greenColor = Colors.green;
}
